<?php

namespace App\Http\Controllers;

use App\Academicyear;
use App\Helpers\AppHelper;
use App\Term;
use Illuminate\Http\Request;
use Session;
use DB;
use Yajra\Datatables\Datatables;

class termController extends Controller
{
    //
    public function index(){
        
        
        return view('term');
    }
    public function allterm(){
        DB::statement(DB::raw('set @rownum=0'));
        $term = Term::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'academicyear_id',
            'name',
            'from',
            'to',
            'status'
        ]);

        $datatables =  Datatables::of($term)

            ->addColumn('action', function ($term) {
                return '
                  <button class="btn btn-warning editbtn" data-id="'.$term->id.'" data-name="'.$term->name.'" data-from="'.$term->from.'" 
                   data-to="'.$term->to.'" 
                   data-status="'.$term->status.'" 
                   data-academicyear_id="'.$term->academicyear_id.'" 
                   data-toggle="modal"  data-target="#editmodal" >Edit </button> |
                  <button class="btn btn-danger deletebtn" data-id="'.$term->id.'" data-name="'.$term->name.'" data-toggle="modal" data-target="#deletemodal">Delete </button>
                  ';
            })
        ->addColumn('academicyear', function ($term) {
            return Term::find($term->id)->name;

        });

        return $datatables->make(true);

    }
    public function save(Request $request){
        if(AppHelper::activeterm()==true){
            Session::flash('error','An Active Term record Already exist');
        }else{
            Term::create($request->all());
            Session::flash('success','Term record Added successfully');
        }
        return redirect('term');  
    }
    public function update(Request $request){
        $term =Term::find($request->id_Edit);
        $term->name = $request->name_Edit;
        $term->from = $request->from_Edit;
        $term->to = $request->to_Edit;
        $term->status = $request->status_Edit;
        $term->save();
        Session::flash('success','Term record updated successfully');
        return redirect('term');
    }
    public function delete(Request $request){
        Term::find($request->id_Delete)->delete();
        Session::flash('success','Term deleted successfully');
        return redirect('term');
    }
    public function academicyeartermselectbox($academicyear_id){
        $output_term='';
        $term_active=DB::table('terms')
            ->join('academicyears', 'academicyears.id', '=', 'terms.academicyear_id')
            ->select('terms.*','academicyears.status')
            ->where('academicyears.status','active')
            ->where('academicyears.id',$academicyear_id)
            ->get();
        foreach ($term_active as $p){
            $output_term.='<option value="'.$p->id.'">'.$p->name.'</option>';
        }

        return Response($output_term);


    }
}
