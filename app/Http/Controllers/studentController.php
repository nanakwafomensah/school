<?php

namespace App\Http\Controllers;

use App\Academicyear;
use App\Stage;
use App\Student;
use App\Term;
use Illuminate\Http\Request;
use Psy\CodeCleaner\StrictTypesPass;
use Image;
use Session;
use Illuminate\Support\Collection;
use Yajra\Datatables\Datatables;
use DB;
class studentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('student');
    }
    public function allstudentindex(){
        return view('allstudent');
    }
    public function allstudent(){
        $students = DB::table('students')->where('status','1')->get();
        $term_id=Term::where('status','active')->first()->id;
        $academicyear_id=Academicyear::where('status','active')->first()->id;
        $data  = [];
        foreach ($students as $w) {
            $obj = new \stdClass;
            $obj->photo = $w->photo;
            $obj->id = $w->id;
            $obj->admdate = $w->signaturedate;
            $obj->child_surname = $w->child_surname;
            $obj->child_firstname = $w->child_firstname;
            $obj->child_middlename = $w->child_middlename;
            $obj->class =Stage::find($w->class_id)->name;
            $obj->gender = $w->gender;
            $obj->dateofbirth = $w->dateofbirth;
            $obj->admission_date = $w->admission_date;
            $obj->religion = $w->religion;
            $obj->housenumber = $w->housenumber;
            $obj->action =  '
                  <a href="student/'.$w->id.'"   class="btn btn-success">Edit</a>
                  <a href="studentdelete/'.$w->id.'"  class="btn btn-danger">Remove</a>
                  <a href="studentbill/'.$w->id.'/'.$w->class_id.'/'.$term_id.'/'.$academicyear_id.'" class="btn btn-warning">Current Bill</a>
                  ';
            $data[] = $obj;
        }

        $student_sorted = new Collection($data);

        return Datatables::of($student_sorted)->make(true);
    }
    public function save(Request $request)
    {


        $student = new Student();
        if($request->hasFile('photo')){

            $photo=$request->file('photo');
            $filename=time().'.'.$photo->getClientOriginalExtension();
            Image::make($photo)->resize(300,300)->save( public_path('/images/'.$filename));

            $student->id=$request->id;
            $student->term_id=$request->term_id;
            $student->academicyear_id=$request->academic_year_id;
            $student->photo=$filename;
            $student->child_surname= $request->child_surname;
            $student->child_firstname= $request->child_firstname;
            $student->child_middlename= $request->child_middlename;
            $student->dateofbirth= $request->dateofbirth;
            $student->admission_date=$request->admission_date;
            $student->religion=$request->religion;
            $student->language=$request->language;
            $student->placeofbirth=$request->placeofbirth;

            $student->housenumber=$request->housenumber;
            $student->town=$request->town;
            $student->surburb=$request->surburb;
            $student->digiaddress=$request->digiaddress;

            $student->class_id=$request->class_id;
            $student->gender=$request->gender;
            $student->nationality=$request->nationality;
            $student->previousdate=$request->previousdate;
            $student->lastschoolattended=$request->lastschoolattended;
            $student->reasons=$request->reasons;
            $student->classsought=$request->classsought;
            $student->father_surname=$request->father_surname;
            $student->father_occupation=$request->father_occupation;
            $student->father_placeofwork=$request->father_placeofwork;
            $student->father_residentialaddress=$request->father_residentialaddress;
            $student->father_othername=$request->father_othername;
            $student->father_telephone=$request->father_telephone;
            $student->father_mobile=$request->father_mobile;
            $student->father_postaladdress=$request->father_postaladdress;
            $student->father_email=$request->father_email;
            $student->mother_surname=$request->mother_surname;
            $student->mother_occupation=$request->mother_occupation;
            $student->mother_placeofwork=$request->mother_placeofwork;
            $student->mother_residentialaddress=$request->mother_residentialaddress;
            $student->mother_othername=$request->mother_othername;
            $student->mother_telephone=$request->mother_telephone;
            $student->mother_mobile=$request->mother_mobile;
            $student->mother_postaladdress=$request->mother_postaladdress;
            $student->mother_email=$request->mother_email;
            $student->guardian_surname=$request->guardian_surname;
            $student->guardian_othername=$request->guardian_othername;
            $student->guardian_telephonenumber=$request->guardian_telephonenumber;
            $student->guardian_email=$request->guardian_email;
            $student->any_disease=$request->any_disease;
            $student->hearing=$request->hearing;
            $student->eyesight=$request->eyesight;
            $student->signature=$request->signature;
            $student->signaturedate=$request->signaturedate;
            $student->status='1';
            $student->save();
        }else{
            $student->id=$request->id;
            $student->child_surname= $request->child_surname;
            $student->child_firstname= $request->child_firstname;
            $student->child_middlename= $request->child_middlename;
            $student->dateofbirth= $request->dateofbirth;
            $student->admission_date=$request->admission_date;
            $student->religion=$request->religion;
            $student->language=$request->language;
            $student->placeofbirth=$request->placeofbirth;

            $student->housenumber=$request->housenumber;
            $student->town=$request->town;
            $student->surburb=$request->surburb;
            $student->digiaddress=$request->digiaddress;

            $student->class=$request->class;
            $student->gender=$request->gender;
            $student->nationality=$request->nationality;
            $student->previousdate=$request->previousdate;
            $student->lastschoolattended=$request->lastschoolattended;
            $student->reasons=$request->reasons;
            $student->classsought=$request->classsought;
            $student->father_surname=$request->father_surname;
            $student->father_occupation=$request->father_occupation;
            $student->father_placeofwork=$request->father_placeofwork;
            $student->father_residentialaddress=$request->father_residentialaddress;
            $student->father_othername=$request->father_othername;
            $student->father_telephone=$request->father_telephone;
            $student->father_mobile=$request->father_mobile;
            $student->father_postaladdress=$request->father_postaladdress;
            $student->father_email=$request->father_email;
            $student->mother_surname=$request->mother_surname;
            $student->mother_occupation=$request->mother_occupation;
            $student->mother_placeofwork=$request->mother_placeofwork;
            $student->mother_residentialaddress=$request->mother_residentialaddress;
            $student->mother_othername=$request->mother_othername;
            $student->mother_telephone=$request->mother_telephone;
            $student->mother_mobile=$request->mother_mobile;
            $student->mother_postaladdress=$request->mother_postaladdress;
            $student->mother_email=$request->mother_email;
            $student->guardian_surname=$request->guardian_surname;
            $student->guardian_othername=$request->guardian_othername;
            $student->guardian_telephonenumber=$request->guardian_telephonenumber;
            $student->guardian_email=$request->guardian_email;
            $student->any_disease=$request->any_disease;
            $student->hearing=$request->hearing;
            $student->eyesight=$request->eyesight;
            $student->signature=$request->signature;
            $student->signaturedate=$request->signaturedate;
            $student->status='1';
            $student->save();
        }
        Session::flash('success','New Student successfully Registered');
        return redirect('newstudent');


    }
    public function edit($student_id)
    {
        return view('editstudent')->with([
            'student'=>Student::find($student_id)->first()
        ]);
    }
    public function update(Request $request)
    {

        $student =Student::find($request->id);
        $student->child_surname= $request->child_surname_Edit;
        $student->child_firstname= $request->child_firstname_Edit;
        $student->child_middlename= $request->child_middlename_Edit;
        $student->dateofbirth= $request->dateofbirth_Edit;
        $student->admission_date=$request->admission_date_Edit;
        $student->religion=$request->religion_Edit;
        $student->language=$request->language_Edit;
        $student->placeofbirth=$request->placeofbirth_Edit;

        $student->housenumber=$request->housenumber_Edit;
        $student->town=$request->town_Edit;
        $student->surburb=$request->surburb_Edit;
        $student->digiaddress=$request->digiaddress_Edit;

        $student->class_id=$request->class_id_Edit;
        $student->gender=$request->gender_Edit;
        $student->nationality=$request->nationality_Edit;
        $student->previousdate=$request->previousdate_Edit;
        $student->lastschoolattended=$request->lastschoolattended_Edit;
        $student->reasons=$request->reasons_Edit;
        $student->classsought=$request->classsought_Edit;
        $student->father_surname=$request->father_surname_Edit;
        $student->father_occupation=$request->father_occupation_Edit;
        $student->father_placeofwork=$request->father_placeofwork_Edit;
        $student->father_residentialaddress=$request->father_residentialaddress_Edit;
        $student->father_othername=$request->father_othername_Edit;
        $student->father_telephone=$request->father_telephone_Edit;
        $student->father_mobile=$request->father_mobile_Edit;
        $student->father_postaladdress=$request->father_postaladdress_Edit;
        $student->father_email=$request->father_email_Edit;
        $student->mother_surname=$request->mother_surname_Edit;
        $student->mother_occupation=$request->mother_occupation_Edit;
        $student->mother_placeofwork=$request->mother_placeofwork_Edit;
        $student->mother_residentialaddress=$request->mother_residentialaddress_Edit;
        $student->mother_othername=$request->mother_othername_Edit;
        $student->mother_telephone=$request->mother_telephone_Edit;
        $student->mother_mobile=$request->mother_mobile_Edit;
        $student->mother_postaladdress=$request->mother_postaladdress_Edit;
        $student->mother_email=$request->mother_email_Edit;
        $student->guardian_surname=$request->guardian_surname_Edit;
        $student->guardian_othername=$request->guardian_othername_Edit;
        $student->guardian_telephonenumber=$request->guardian_telephonenumber_Edit;
        $student->guardian_email=$request->guardian_email_Edit;
        $student->any_disease=$request->any_disease_Edit;
        $student->hearing=$request->hearing_Edit;
        $student->eyesight=$request->eyesight_Edit;
        $student->signature=$request->signature_Edit;
        $student->signaturedate=$request->signaturedate_Edit;
        $student->status='1';
        $student->save();
        Session::flash('success','Student Details successfully Updated');
        return redirect('students');
    }

   
    public function destroy($student_id)
    {
        $student =Student::find($student_id);
        $student->status = '0';
        $student->save();
        Session::flash('success','Student removed from school successfully');
        return redirect('students');
        //
    }
}
