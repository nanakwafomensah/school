<?php

namespace App\Http\Controllers;

use App\Academicyear;
use App\Feeitem;
use App\Student;
use App\Term;
use Illuminate\Http\Request;

class billController extends Controller
{
    //
    public function studentbill($student_id,$class_id,$term_id,$academicyear_id){
        $feeitem=Feeitem::where('class_id',$class_id)->where('term_id',$term_id)->where('academicyear_id',$academicyear_id)->get();
        return view('studentbill')->with([
            'feeitem'=>$feeitem,
            'studentnumber'=>$student_id,
            'studentname'=>Student::find($student_id)->child_surname  .' '. Student::find($student_id)->child_firstname .' '.Student::find($student_id)->child_middlename,
            'academicyear'=>Academicyear::find($academicyear_id)->name,
            'term'=>Term::find($term_id)->name
        ]);
    }
}
