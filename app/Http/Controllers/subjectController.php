<?php

namespace App\Http\Controllers;

use App\Stage;
use App\Subject;
use Illuminate\Http\Request;
use Sentinel;
use Yajra\Datatables\Datatables;
use Session;
use DB;
class subjectController extends Controller
{
    //
    public function index(){
        return view('subject');
    }
    public function allsubject(){
        DB::statement(DB::raw('set @rownum=0'));
        $subject = Subject::select([ DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id', 'name', 'class_id', 'subject_id'
        ]);

        $datatables =  Datatables::of($subject)

            ->addColumn('action', function ($subject) {
                return '
                  <button  class="btn btn-warning editbtn" data-id="'.$subject->id.'" data-name="'.$subject->name.'" data-class_id="'.$subject->class_id.'" data-subject_id="'.$subject->subject_id.'" data-toggle="modal"  data-target="#editmodal" >Edit </button> |
                  <button  class="btn btn-danger deletebtn" data-id="'.$subject->id.'" data-name="'.$subject->name.'" data-class_id="'.$subject->class_id.'" data-subject_id="'.$subject->subject_id.'" data-toggle="modal" data-target="#deletemodal">Delete</button>
                  ';
            })
        ->addColumn('class', function ($subject) {
            return Stage::find($subject->id)->name;

        });


        return $datatables->make(true);

    }
    public function save(Request $request){
        Subject::create($request->all());
        Session::flash('success','Subject record Added successfully');
        return redirect('subject');
    }
    public function update(Request $request){
//        dd($request->all());
        $subject =Subject::find($request->id_Edit);
        $subject->class_id = $request->class_id_Edit;
        $subject->subject_id = $request->subjectcode_Edit;
        $subject->name = $request->name_Edit;
        $subject->save();
        Session::flash('success','Subject record updated successfully');
        return redirect('subject'); 
    }
    public function delete(Request $request){
        Subject::find($request->id_Delete)->delete();
        Session::flash('success','Subject deleted successfully');
        return redirect('subject');
    }
}
