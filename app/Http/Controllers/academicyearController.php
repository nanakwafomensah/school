<?php

namespace App\Http\Controllers;

use App\Academicyear;
use App\Helpers\AppHelper;
use Illuminate\Http\Request;
use Session;
use DB;
use Yajra\Datatables\Datatables;
class academicyearController extends Controller
{
    //
    public function index(){

        return view('academicyear');
    }
    public function allacademicyear(){
        DB::statement(DB::raw('set @rownum=0'));
        $academicyear = Academicyear::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'name','from','to','status'
        ]);

        $datatables =  Datatables::of($academicyear)

            ->addColumn('action', function ($academicyear) {
                return '
                  <button href="#" class="btn btn-warning editbtn" data-id="'.$academicyear->id.'" data-name="'.$academicyear->name.'" data-from="'.$academicyear->from.'"  data-to="'.$academicyear->to.'" data-status="'.$academicyear->status.'" data-toggle="modal"  data-target="#editmodal" >Edit </button> |
                  <button href="#" class="btn btn-danger deletebtn" data-id="'.$academicyear->id.'" data-name="'.$academicyear->name.'" data-toggle="modal" data-target="#deletemodal">Delete </button>
                  ';
            });


        return $datatables->make(true);

    }
    public function save(Request $request){
        if(AppHelper::activeacademicyear()==true){

            Session::flash('success','An Active Academic Year already exit');
        }else{
            Academicyear::create($request->all());
            Session::flash('success','Academic Year record Added successfully');
        }
        
        return redirect('academicyear');
    }
    public function update(Request $request){
        $academicyear =Academicyear::find($request->id_Edit);
        $academicyear->name = $request->name_Edit;
        $academicyear->from = $request->from_Edit;
        $academicyear->to = $request->to_Edit;
        $academicyear->status = $request->status_Edit;
        $academicyear->save();
        Session::flash('success','Academic Year record updated successfully');
        return redirect('academicyear');
    }
    public function delete(Request $request){
        Academicyear::find($request->id_Delete)->delete();
        Session::flash('success','Academic Year deleted successfully');
        return redirect('academicyear');

    }
}
