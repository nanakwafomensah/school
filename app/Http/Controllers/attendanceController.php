<?php

namespace App\Http\Controllers;

use App\Academicyear;
use App\Stage;
use App\Student;
use App\Studentattendance;
use App\Teacherclass;
use App\Term;
use Sentinel;
use Illuminate\Http\Request;
use DB;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Collection;
class attendanceController extends Controller
{
    //
    public function studentattendance(){
        $students=null;
        if(Sentinel::getUser()->roles()->first()->slug=='Teacher')
        {   $user_id=Sentinel::getUser()->getUserId();
            $class_assigned= Teacherclass::find($user_id)->class_id;
            $students= Student::where('status',1)->where('class_id',$class_assigned)->get();
            //have to add Academic year id to Student
        }elseif (Sentinel::getUser()->roles()->first()->slug=='Admin'){
            $students= Student::where('status',1)->get();
        }
        return view('studentattendance')->with([
            'students'=>$students,
            'attendance_date'=>date('D, M j, Y \a\t g:ia')
        ]);
    }


    public function allstudentattendance()
    {
        DB::statement(DB::raw('set @rownum=0'));
        $student = Student::select([DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'id','child_surname','child_firstname','child_middlename','class_id','photo'])->where('status',1)->get();

        $data  = [];
        foreach ($student as $w) {
            $obj = new \stdClass;
            $obj->rownum = $w->id;
            $obj->name = $w->child_firstname ." ".$w->child_middlename." ".$w->child_surname;
            $obj->class = Stage::find($w->class_id)->name;
            $obj->photo = '<img src="images/'.$w->photo.'" width="70px" height="70px" />';
            $obj->attendance_action = '
             <center>Present: <label class="switch">
             <input type="checkbox" value="Yes" class="success" style="margin-top: 25px;" id="attendance_status" name="'.$w->id.'">
             <span class="slider round"></span> </label></center>  
             <input type="hidden" value="No" name="'.$w->id.'" id="attendance_statusHidden">
         
              ';
            $data[] = $obj;
        }
        $student_sorted = new Collection($data);

        return Datatables::of($student_sorted)->escapeColumns([])->make(true);
    }

    public function save(Request $request){

        $attendance_list=$request->all();
        $filtered_attendance_list = array_filter($attendance_list, function($k){
            return preg_match('/BIOT/', $k);
        }, ARRAY_FILTER_USE_KEY);

        foreach ($filtered_attendance_list as $data=>$value)
        {
            $studentAttendance = new Studentattendance();
            $studentAttendance->student_id =$data;
            $studentAttendance->status =$value;
            $studentAttendance->attendance_date =date('Y-m-d');
            $studentAttendance->class_id =  Student::find($data)->class_id;
            $studentAttendance->academicyear_id =Academicyear::where('status','active')->first()->id;
            $studentAttendance->term_id =Term::where('status','active')->first()->id;
            $studentAttendance->save();
        }
        return redirect('studentattendance');
    }
    
    public function allstudentattendanceview(){
        
        return view('studentattendancereport');
    }
    public function allstudentattendancedata(Request $request){
        if ($request->filled('academicyear')) {
            $attendanceitems = Studentattendance::where('academicyear_id',$request->academicyear)
                ->where('term_id',$request->term)
                ->where('class_id',$request->stage)
                ->where('attendance_date','>=',$request->fromdate)
                ->where('attendance_date','<=',$request->todate)
                ->get();
        }else{
            $attendanceitems =DB::table('studentattendances')->get();
        }
        $data  = [];

        foreach ($attendanceitems as $w) {
            $obj = new \stdClass;
            $obj->attendancedate = $w->attendance_date;
            $obj->student = Student::find($w->student_id)->child_surname.' '.Student::find($w->student_id)->child_firstname.' '.Student::find($w->student_id)->child_middlename;
            $obj->academicyear = Academicyear::find($w->academicyear_id)->name;
            $obj->term = Term::find($w->term_id)->name;
            $obj->stage = Stage::find($w->class_id)->name;
            $obj->status = ($w->status=='Yes')?'<i class="fa fa-check fa-2x" style="color:green" aria-hidden="true"></i>':'<i class="fa fa-times fa-2x" aria-hidden="true" style="color:red"></i>';
            $data[] = $obj;
        }

        $feeitems_sorted = new Collection($data);

        return Datatables::of($feeitems_sorted)->escapeColumns([])->make(true);
    }

}
