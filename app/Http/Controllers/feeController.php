<?php

namespace App\Http\Controllers;

use App\Academicyear;
use App\Feeitem;
use App\Stage;
use App\Term;
use Illuminate\Http\Request;
use DB;
use Session;
use Illuminate\Support\Collection;
use Yajra\Datatables\Datatables;
class feeController extends Controller
{
    //
    public function index(){
        return view('fee');
    }
    public function allfee(){
        return view('allfee');
    }
    public function allfees(Request $request){
        if ($request->filled('academicyear')) {

            $feeitems = Feeitem::where('academicyear_id',$request->academicyear)
                ->where('term_id',$request->term)
                ->where('class_id',$request->stage)
                ->get();
        }else{
            $feeitems =DB::table('feeitems')->get();
        }

        $data  = [];

        foreach ($feeitems as $w) {
            $obj = new \stdClass;
            $obj->feeitem = $w->name;
            $obj->amount = "GHC ". $w->amount;
            $obj->academicyear = Academicyear::find($w->academicyear_id)->name;
            $obj->term = Term::find($w->term_id)->name;
            $obj->stage = Stage::find($w->class_id)->name;
            $obj->date = date($w->created_at);
            $obj->action = '<a href="deletefee/'.$w->id.'"  class="btn btn-danger">Unapply</a>';
            $data[] = $obj;
        }

        $feeitems_sorted = new Collection($data);

        return Datatables::of($feeitems_sorted)->make(true);
    }
    public function save(Request $request){
        //dd(count($request->class_id));
        $number_of_items=$request->number_of_items;
        if($number_of_items > 0){
            for($c=0;$c < count($request->class_id);$c++) {
                   // dd($request->class_id[$c]);
                for ($i = 0; $i < $number_of_items; $i++) {
                    $item = new Feeitem();
                    $item->name = $request->item[$i];
                    $item->amount = $request->amount[$i];
                    $item->term_id = $request->term_id;
                    $item->academicyear_id = $request->academicyear_id;
                    $item->class_id = $request->class_id[$c];
                    $item->save();
                }
            }
            Session::flash('success','Bills  placed successfully ');
        }else{
            Session::flash('error','No Item was Added');
        }
        return redirect('fee');
    }

    public function destroy($fee_id)
    {
        Feeitem::find($fee_id)->delete();


        return redirect('allfee');
        //
    }
}
