<?php

namespace App\Http\Controllers;

use App\Stage;
use App\Teacherclass;
use Cartalyst\Sentinel\Users\EloquentUser;
use Illuminate\Http\Request;
use Sentinel;
use Session;
use Yajra\Datatables\Datatables;
use DB;
class teacherclassController extends Controller
{
    //
    public function index(){
        
        return view('teacherclass');
    }
    public function allteacherclass(){
        DB::statement(DB::raw('set @rownum=0'));
        $teacherclass = Teacherclass::select([ DB::raw('@rownum  := @rownum  + 1 AS rownum'), 'user_id', 'class_id'
        ]);

        $datatables =  Datatables::of($teacherclass)

            ->addColumn('action', function ($teacherclass) {
                return '
                  <button  class="btn btn-warning editbtn" data-user_id="'.$teacherclass->user_id.'" data-class_id="'.$teacherclass->class_id.'"   data-toggle="modal"  data-target="#editmodal" >Edit </button> |
                  <button  class="btn btn-danger deletebtn" data-user_id="'. EloquentUser::find($teacherclass->user_id)->first_name.' '.EloquentUser::find($teacherclass->user_id)->last_name.'"   data-toggle="modal" data-target="#deletemodal">Delete</button>
                  ';
            })
            ->addColumn('teacher', function ($teacherclass) {
                return EloquentUser::find($teacherclass->user_id)->first_name.' '.EloquentUser::find($teacherclass->user_id)->last_name;

            })
        ->addColumn('class', function ($teacherclass) {
            return Stage::find($teacherclass->class_id)->name;

        });


        return $datatables->make(true);
    }
    public  function save(Request $request){
        Teacherclass::create($request->all());
        Session::flash('success','Teacher Assigned to class successfully');
        return redirect('teacherclass');
    }
    public function update(Request $request){
        $subject =Teacherclass::find($request->user_id_Edit);
        $subject->class_id = $request->class_id_Edit;
        $subject->save();
        Session::flash('success','Record updated successfully');
        return redirect('teacherclass');
    }
    public function delete(Request $request){
        Teacherclass::find($request->user_id_Delete)->delete();
        Session::flash('success','Record deleted successfully');
        return redirect('teacherclass');
    }
    
}
