<?php

namespace App\Http\Controllers;

use App\Stage;
use Illuminate\Http\Request;
use Sentinel;
use Yajra\Datatables\Datatables;
use Session;
use DB;
class StageController extends Controller
{
    //
    public function index(){
        return view('class');
    }
    public function allclass(){
        DB::statement(DB::raw('set @rownum=0'));
        $class = Stage::select([
            DB::raw('@rownum  := @rownum  + 1 AS rownum'),
            'id',
            'name',
            'capacity'
        ]);

        $datatables =  Datatables::of($class)

            ->addColumn('action', function ($class) {
                return '
                  <button class="btn btn-warning editbtn" data-id="'.$class->id.'" data-name="'.$class->name.'" data-capacity="'.$class->capacity.'" data-toggle="modal"  data-target="#editmodal" >Edit</button> |
                  <button  class="btn btn-danger deletebtn" data-id="'.$class->id.'" data-name="'.$class->name.'" data-capacity="'.$class->capacity.'" data-toggle="modal" data-target="#deletemodal">Delete</button>
                  ';
            });


        return $datatables->make(true);

    }
    public function save(Request $request){
        Stage::create($request->all());
        Session::flash('success','Class record Added successfully');
        return redirect('class');
    }
    public function update(Request $request){

        
        $class =Stage::find($request->id_Edit);
        $class->name = $request->name_Edit;
        $class->capacity = $request->capacity_Edit;
        $class->save();
        Session::flash('success','Class record updated successfully');
        return redirect('class');
    }
    public function delete(Request $request){
        Stage::find($request->id_Delete)->delete();
        Session::flash('success','Class deleted successfully');
        return redirect('class');
    }
}
