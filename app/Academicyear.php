<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Academicyear extends Model
{
    //

    protected $fillable=['name','from','to','status'];
}
