<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feeitem extends Model
{
    //
    protected $fillable =['name','amount','term_id','class_id','academicyear_id'];
}
