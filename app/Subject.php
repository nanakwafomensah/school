<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    //
//    protected $primaryKey = 'subject_id';

//    public $incrementing = false;
    
    protected $fillable=['class_id','subject_id','name'];
}
