<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Studentattendance extends Model
{
    //
    protected $fillable =['status','attendance_date','student_id','class_id','academicyear_id','term_id'];
    
}
