<?php
namespace App\Helpers;


use App\Academicyear;
use App\Student;
use App\Term;
use Illuminate\Support\Collection;
use DB;
use Sentinel;
class AppHelper
{
    public static function profileimage()
    {
        return  DB::table('profiles')->first();

    }
    public static function admissionnumber()
    {
        $admissionnumber=null;
        $studentnumber=Student::count();
        if($studentnumber == 0){
            $admissionnumber = 'BIOT-'. 1;
        }else{
            $studentnumber=$studentnumber+1;
            $admissionnumber = 'BIOT-'. $studentnumber;
        }

        return  $admissionnumber;

    }
    public static function activeterm(){
        $activeterm=Term::where('status','active')->count();
        if($activeterm > 0)  
            return true ;
        else 
            return false;
        
    }
    public static function activeacademicyear(){
        $activeacademicyear=Academicyear::where('status','active')->count();
        if($activeacademicyear > 0)
            return true ;
        else
            return false;

    }

public static function getactiveterm(){
    return Term::where('status','active')->first()->name;
}
    public static function getactiveacademicyear(){
        return Academicyear::where('status','active')->first()->name;
    }
    //return users with role teacher
    public  static function teacheruser(){
        return Sentinel::findRoleBySlug('Teacher')->users()->with('roles')->get();
       
    }

}