<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacherclass extends Model
{
    //
    protected $primaryKey = 'user_id';

    public $incrementing = false;
    protected $fillable=['user_id','class_id'];
}
