<?php
/**
 * Created by PhpStorm.
 * User: kwafo
 * Date: 2/23/2019
 * Time: 1:47 PM
 */
$factory->define(App\Profile::class, function (Faker\Generator $faker) {
    return [
        'companyname'=>$faker->company,
        'phone'=>'0878999',
        'email'=>'nanamene@ghs.com',
        'address'=>$faker->address,
        'mobile'=>'343332',
        'website'=>'www.nanakwafomensah.info',
        'fax'=>'223344',
        'logo'=>'default.jpg'
    ];
});