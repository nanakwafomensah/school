<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->string('id')->primary();
            $table->string('academicyear_id')->nullable($value = true);
            $table->string('term_id')->nullable($value = true);
            $table->string('photo')->nullable($value = true);
            $table->string('child_surname')->nullable($value = false);
            $table->string('child_firstname')->nullable($value = false);
            $table->string('child_middlename')->nullable($value = false);
            $table->string('admission_date')->nullable($value = false);
            $table->string('religion')->nullable($value = true);
            $table->string('town')->nullable($value = true);
            $table->string('housenumber')->nullable($value = true);
            $table->string('surburb')->nullable($value = true);
            $table->string('digiaddress')->nullable($value = true);
            $table->string('class_id')->nullable($value = false);
            $table->string('dateofbirth')->nullable($value = true);
            $table->string('language')->nullable($value = true);
            $table->string('gender')->nullable($value = true);
            $table->string('placeofbirth')->nullable($value = true);
            $table->string('nationality')->nullable($value = true);
            $table->string('previousdate')->nullable($value = true);
            $table->string('lastschoolattended')->nullable($value = true);
            $table->string('reasons')->nullable($value = true);
            $table->string('classsought')->nullable($value = true);
            $table->string('father_surname')->nullable($value = true);
            $table->string('father_othername')->nullable($value = true);
            $table->string('father_occupation')->nullable($value = true);
            $table->string('father_telephone')->nullable($value = true);
            $table->string('father_placeofwork')->nullable($value = true);
            $table->string('father_mobile')->nullable($value = true);
            $table->string('father_residentialaddress')->nullable($value = true);
            $table->string('father_postaladdress')->nullable($value = true);
            $table->string('father_email')->nullable($value = true);

            $table->string('mother_surname')->nullable($value = true);
            $table->string('mother_othername')->nullable($value = true);
            $table->string('mother_occupation')->nullable($value = true);
            $table->string('mother_telephone')->nullable($value = true);
            $table->string('mother_placeofwork')->nullable($value = true);
            $table->string('mother_mobile')->nullable($value = true);
            $table->string('mother_residentialaddress')->nullable($value = true);
            $table->string('mother_postaladdress')->nullable($value = true);
            $table->string('mother_email')->nullable($value = true);

            $table->string('guardian_surname')->nullable($value = true);
            $table->string('guardian_othername')->nullable($value = true);
            $table->string('guardian_telephonenumber')->nullable($value = true);
            $table->string('guardian_email')->nullable($value = true);


            $table->string('any_disease')->nullable($value = true);
            $table->string('hearing')->nullable($value = true);
            $table->string('eyesight')->nullable($value = true);
            $table->string('signature');
            $table->string('signaturedate');
            $table->string('status');


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
