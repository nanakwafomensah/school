<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('login');
});
Route::get('dashboard',['as'=>'dashboard','uses'=>'dashboardController@index']);
Route::get('newstudent',['as'=>'newstudent','uses'=>'studentController@index']);
Route::get('studentattendance',['as'=>'studentattendance','uses'=>'attendanceController@studentattendance']);
Route::get('student/{student_id}',['as'=>'student','uses'=>'studentController@edit']);
Route::get('studentdelete/{student_id}',['as'=>'studentdelete','uses'=>'studentController@destroy']);
Route::get('deletefee/{fee_id}',['as'=>'deletefee','uses'=>'feeController@destroy']);
Route::get('studentbill/{student_id}/{class_id}/{term_id}/{academicyear_id}',['as'=>'studentbill','uses'=>'billController@studentbill']);
Route::get('students',['as'=>'students','uses'=>'studentController@allstudentindex']);
Route::get('academicyear',['as'=>'academicyear','uses'=>'academicyearController@index']);
Route::get('profile',['as'=>'profile','uses'=>'profileController@index']);
Route::get('term',['as'=>'term','uses'=>'termController@index']);
Route::get('user',['as'=>'user','uses'=>'userController@index']);
Route::get('role',['as'=>'role','uses'=>'roleController@index']);
Route::get('class',['as'=>'class','uses'=>'stageController@index']);
Route::get('subject',['as'=>'subject','uses'=>'subjectController@index']);
Route::get('fee',['as'=>'fee','uses'=>'feeController@index']);
Route::get('teacherclass',['as'=>'teacherclass','uses'=>'teacherclassController@index']);
Route::get('allfee',['as'=>'allfee','uses'=>'feeController@allfee']);
Route::get('allstudentattendanceview',['as'=>'allstudentattendanceview','uses'=>'attendanceController@allstudentattendanceview']);
Route::get('academicyeartermselectbox/{academicyear_id}',['as'=>'academicyeartermselectbox','uses'=>'termController@academicyeartermselectbox']);



Route::post('login',['as'=>'login','uses'=>'loginController@login']);
Route::post('logout',['as'=>'logout','uses'=>'loginController@logout']);

Route::post('savefee',['as'=>'savefee','uses'=>'feeController@save']);
Route::post('updatefee',['as'=>'updatefee','uses'=>'feeController@update']);
Route::post('deletefee',['as'=>'deletefee','uses'=>'feeController@delete']);

Route::post('savesubject',['as'=>'savesubject','uses'=>'subjectController@save']);
Route::post('updatesubject',['as'=>'updatesubject','uses'=>'subjectController@update']);
Route::post('deletesubject',['as'=>'deletesubject','uses'=>'subjectController@delete']);

Route::post('saveclass',['as'=>'saveclass','uses'=>'stageController@save']);
Route::post('updateclass',['as'=>'updateclass','uses'=>'stageController@update']);
Route::post('deleteclass',['as'=>'deleteclass','uses'=>'stageController@delete']);

Route::post('saveterm',['as'=>'saveterm','uses'=>'termController@save']);
Route::post('updateterm',['as'=>'updateterm','uses'=>'termController@update']);
Route::post('deleteterm',['as'=>'deleteterm','uses'=>'termController@delete']);

Route::post('savestudent',['as'=>'savestudent','uses'=>'studentController@save']);
Route::post('updatestudent',['as'=>'updatestudent','uses'=>'studentController@update']);
Route::post('deletestudent',['as'=>'deletestudent','uses'=>'studentController@delete']);

Route::post('updateschoolsetup',['as'=>'updateschoolsetup','uses'=>'profileController@update']);

Route::post('saveteacherclass',['as'=>'saveteacherclass','uses'=>'teacherclassController@save']);
Route::post('updateteacherclass',['as'=>'updateteacherclass','uses'=>'teacherclassController@update']);
Route::post('deleteteacherclass',['as'=>'deleteteacherclass','uses'=>'teacherclassController@delete']);

Route::post('saveschoolsetup',['as'=>'saveschoolsetup','uses'=>'profileController@save']);
Route::post('updateschoolsetup',['as'=>'updateschoolsetup','uses'=>'profileController@update']);
Route::post('deleteschoolsetup',['as'=>'deleteschoolsetup','uses'=>'profileController@delete']);

Route::post('saveacademicyear',array('as'=>'saveacademicyear','uses'=>'academicyearController@save'));
Route::post('updateacademicyear',array('as'=>'updateacademicyear','uses'=>'academicyearController@update'));
Route::post('deleteacademicyear',array('as'=>'deleteacademicyear','uses'=>'academicyearController@delete'));

Route::post('adduser',['as'=>'adduser','uses'=>'userController@adduser']);
Route::post('edituser',['as'=>'edituser','uses'=>'userController@edituser']);
Route::post('deleteuser',['as'=>'deleteuser','uses'=>'userController@deleteuser']);

Route::post('saveattendance',array('as'=>'saveattendance','uses'=>'attendanceController@save'));

Route::post('newrole',['as'=>'newrole','uses'=>'roleController@newrole']);
Route::post('deleterole',['as'=>'deleterole','uses'=>'roleController@delete']);


Route::get('allstudent',array('as'=>'allstudent','uses'=>'studentController@allstudent'));
Route::get('allacademicyear',array('as'=>'allacademicyear','uses'=>'academicyearController@allacademicyear'));
Route::get('allterm',array('as'=>'allterm','uses'=>'termController@allterm'));
Route::get('allusers',array('as'=>'allusers','uses'=>'userController@allusers'));
Route::get('allroles',array('as'=>'allroles','uses'=>'roleController@allroles'));
Route::get('allclass',array('as'=>'allclass','uses'=>'stageController@allclass'));
Route::get('allsubject',array('as'=>'allsubject','uses'=>'subjectController@allsubject'));
Route::get('allfees',array('as'=>'allfees','uses'=>'feeController@allfees'));
Route::get('allstudentattendancedata',array('as'=>'allstudentattendancedata','uses'=>'attendanceController@allstudentattendancedata'));
Route::get('allteacherclass',array('as'=>'allteacherclass','uses'=>'teacherclassController@allteacherclass'));
Route::get('allstudentattendance',array('as'=>'allstudentattendance','uses'=>'attendanceController@allstudentattendance'));
  