<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/datepicker/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">

</head>
<body>
<section class="vbox">
    @include('partials.header')
    <section class="hbox stretch">
        @include('partials.navbar')
                <!-- /.aside -->
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">

                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="student"><i class="fa fa-home"></i>Student</a></li>
                            </ul>
                            @include('partials.messages')
                            <form method="post" action="{{ route('updatestudent') }}" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <div class="row">
                                    <div class="col-md-12">
                                        <section class="panel panel-default">
                                            <header class="panel-heading font-bold">
                                                <a data-toggle="collapse" href="#collapse0"><span class="label label-primary">Step 1</span>&nbsp;Student photo</a>
                                            </header>
                                            <div id="collapse0" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <input type="file" id="imgInp" name="photo_Edit" value="/school/public/images/{{$student->photo}}" class="form-control"><br>
                                                            @if(!empty($student->photo))
                                                                <div><img id="preview_image" src="/school/public/images/{{$student->photo}}" width="300px" height="300px" class="img-responsive text-center" ></div>
                                                            @else
                                                                <div><img src="images/wo.jpg" width="270px" height="270px" class="img-responsive text-center" ></div>

                                                            @endif

                                                        </div>


                                                    </div>


                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                </div>
                                <input value="{{$student->id}}" name="id" type="hidden"/>
                                <div class="row">
                                    <div class="col-md-12">
                                        <section class="panel panel-default">
                                            <header class="panel-heading font-bold">
                                                <a data-toggle="collapse" href="#collapse1"><span class="label label-primary">Step 2</span>&nbsp; Student Details</a>
                                            </header>
                                            <div id="collapse1" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-md-4">

                                                        <div class="form-group">
                                                            <label for="" style="color:red"> SURNAME *</label>
                                                            <input type="text" id="child_surname" class="form-control" name="child_surname_Edit" value="{{$student->child_surname}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" style="color:red">FIRST NAME *</label>
                                                            <input type="text" id="child_firstname" class="form-control" name="child_firstname_Edit" value="{{$student->child_firstname}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" style="color:red">MIDDLE NAME *</label>
                                                            <input type="text" id="child_middlename" class="form-control" name="child_middlename_Edit" value="{{$student->child_middlename}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">DATE OF BIRTH</label>
                                                            <input type="date" id="dateofbirth" class="form-control" name="dateofbirth_Edit" value="{{$student->dateofbirth}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""> Town</label>
                                                            <input type="text" id="town" class="form-control" name="town_Edit" value="{{$student->town}}" >
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">

                                                        <div class="form-group">
                                                            <label for="" style="color:red">ADMISSION DATE  *</label>
                                                            <input type="date" id="admission_date" class="form-control" name="admission_date_Edit" value="{{$student->admission_date}}"  >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">RELIGION</label>
                                                            <input type="text" id="religion" class="form-control" name="religion_Edit" value="{{$student->religion}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">LANGUAGES SPOKEN</label>
                                                            <input type="text" id="language" class="form-control" name="language_Edit" value="{{$student->language}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">PLACE OF BIRTH</label>
                                                            <input type="text" id="placeofbirth" class="form-control" name="placeofbirth_Edit" value="{{$student->placeofbirth}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""> HSe number</label>
                                                            <input type="text" id="housenumber" class="form-control" name="housenumber_Edit" value="{{$student->housenumber}}" >
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">



                                                        <div class="form-group">
                                                            <label for=""> Surburb</label>
                                                            <input type="text" id="surburb" class="form-control" name="surburb_Edit" value="{{$student->surburb}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for=""> Digital Address</label>
                                                            <input type="text" id="digiaddress" class="form-control" name="digiaddress_Edit" value="{{$student->digiaddress}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" style="color:red">CLASS *</label>
                                                            <select name="class_id_Edit" id="class_id" class="form-control">
                                                                <option value="">select</option>
                                                                @foreach(\App\Stage::all() as $s)
                                                                    @if($s->id == $student->class_id)
                                                                    <option  selected value="{{$s->id}}">{{$s->name}}</option>
                                                                    @else
                                                                        <option  selected value="{{$s->id}}">{{$s->name}}</option>
                                                                        @endif
                                                                @endforeach
                                                            </select>   </div>
                                                        <div class="form-group">
                                                            <label for="">GENDER</label>
                                                            <select name="gender_Edit" id="gender_Edit" class="form-control">
                                                                <option value="">select</option>
                                                                @if($student->gender=='male')
                                                                <option selected value="male">MALE</option>
                                                                @else
                                                                    <option value="male">MALE</option>

                                                                @endif
                                                                @if($student->gender=='female')
                                                                    <option selected value="female">FEMALE</option>
                                                                @else
                                                                    <option value="female">FEMALE</option>

                                                                @endif

                                                            </select> </div>
                                                        <div class="form-group">
                                                            <label for="">NATIONALITY</label>
                                                            <input type="text" id="nationality" class="form-control" name="nationality_Edit" value="{{$student->nationality}}">
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <section class="panel panel-default">
                                            <header class="panel-heading font-bold">
                                                <a data-toggle="collapse" href="#collapse2"><span class="label label-primary">Step 3</span>&nbsp;  Previous School Attended</a>
                                            </header>
                                            <div id="collapse2" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="">DATE</label>
                                                            <input type="date" id="previousdate" class="form-control" name="previousdate_Edit" value="{{$student->previousdate}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">LAST SCHOOL ATTENDED</label>
                                                            <input type="text" id="lastschoolattended" class="form-control" name="lastschoolattended_Edit" value="{{$student->lastschoolattended}}" >
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="">REASONS</label>
                                                            <input type="text" id="reasons" class="form-control" name="reasons_Edit" value="{{$student->reasons}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">CLASS SOUGHT</label>
                                                            <input type="text" id="classsought" class="form-control" name="classsought_Edit" value="{{$student->classsought}}" >
                                                        </div>


                                                    </div>

                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <section class="panel panel-default">
                                            <header class="panel-heading font-bold">
                                                <a data-toggle="collapse" href="#collapse3"> <span class="label label-primary">Step 4</span>&nbsp;  Father Details</a>
                                            </header>
                                            <div id="collapse3" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="">SURNAME</label>
                                                            <input type="text" id="father_surname" class="form-control" name="father_surname_Edit" value="{{$student->father_surname}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">OCCUPATION</label>
                                                            <input type="text" id="father_occupation" class="form-control" name="father_occupation_Edit" value="{{$student->father_occupation}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">PLACE OF WORK</label>
                                                            <input type="text" id="father_placeofwork" class="form-control" name="father_placeofwork_Edit" value="{{$student->father_placeofwork}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">RESIDENTIAL ADDRESS</label>
                                                            <input type="text" id="father_residentialaddress" class="form-control" name="father_residentialaddress_Edit" value="{{$student->father_residentialaddress}}" >
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="">OTHER NAMES</label>
                                                            <input type="text" id="father_othername" class="form-control" name="father_othername_Edit" value="{{$student->father_othername}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">TELEPHONE NUMBER</label>
                                                            <input type="text" id="father_telephone" class="form-control" name="father_telephone_Edit" value="{{$student->father_telephone}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">MOBILE NO.</label>
                                                            <input type="text" id="father_mobile" class="form-control" name="father_mobile_Edit" value="{{$student->father_mobile}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">POSTAL ADDRESS</label>
                                                            <input type="text" id="father_postaladdress" class="form-control" name="father_postaladdress_Edit" value="{{$student->father_postaladdress}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">EMAIL</label>
                                                            <input type="email" id="father_email" class="form-control" name="father_email_Edit" value="{{$student->father_email}}" >
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <section class="panel panel-default">
                                            <header class="panel-heading font-bold">
                                                <a data-toggle="collapse" href="#collapse4"><span class="label label-primary">Step 5</span>&nbsp; Mother Details</a>
                                            </header>
                                            <div id="collapse4" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="">SURNAME</label>
                                                            <input type="text" id="mother_surname" class="form-control" name="mother_surname_Edit" value="{{$student->mother_surname}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">OCCUPATION</label>
                                                            <input type="text" id="mother_occupation" class="form-control" name="mother_occupation_Edit" value="{{$student->mother_occupation}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">PLACE OF WORK</label>
                                                            <input type="text" id="mother_placeofwork" class="form-control" name="mother_placeofwork_Edit" value="{{$student->mother_placeofwork}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">RESIDENTIAL ADDRESS</label>
                                                            <input type="text" id="mother_residentialaddress" class="form-control" name="mother_residentialaddress_Edit" value="{{$student->mother_residentialaddress}}" >
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="">OTHER NAMES</label>
                                                            <input type="text" id="mother_othername" class="form-control" name="mother_othername_Edit" value="{{$student->mother_othername}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">TELEPHONE NUMBER</label>
                                                            <input type="text" id="mother_telephone" class="form-control" name="mother_telephone_Edit" value="{{$student->mother_telephone}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">MOBILE NO.</label>
                                                            <input type="text" id="mother_mobile" class="form-control" name="mother_mobile_Edit"  value="{{$student->mother_mobile}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">POSTAL ADDRESS</label>
                                                            <input type="text" id="mother_postaladdress" class="form-control" name="mother_postaladdress_Edit" value="{{$student->mother_postaladdress}}" >
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">EMAIL</label>
                                                            <input type="email" id="mother_email" class="form-control" name="mother_email_Edit" value="{{$student->mother_email}}" >
                                                        </div>

                                                    </div>

                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <section class="panel panel-default">
                                            <header class="panel-heading font-bold">
                                                <a data-toggle="collapse" href="#collapse5"><span class="label label-primary">Step 6</span>&nbsp;Guardian Details</a>
                                            </header>
                                            <div id="collapse5" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-md-4">

                                                        <div class="form-group">
                                                            <label for="">SURNAME</label>
                                                            <input type="text" id="guardian_surname" class="form-control" name="guardian_surname_Edit" value="{{$student->guardian_surname}}" >
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">

                                                        <div class="form-group">
                                                            <label for="">OTHER NAME</label>
                                                            <input type="text" id="guardian_othername" class="form-control" name="guardian_othername_Edit" value="{{$student->guardian_othername}}" >
                                                        </div>

                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">TELEPHONE NUMBER</label>
                                                            <input type="text" id="guardian_telephonenumber" class="form-control" name="guardian_telephonenumber_Edit" value="{{$student->guardian_telephonenumber}}" >
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="form-group">
                                                            <label for="">Email</label>
                                                            <input type="text" id="guardian_email" class="form-control" name="guardian_email_Edit" value="{{$student->guardian_email}}" >
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <section class="panel panel-default">
                                            <header class="panel-heading font-bold">
                                                <a data-toggle="collapse" href="#collapse7"><span class="label label-primary">Step 8</span>&nbsp;Medical Details</a>
                                            </header>
                                            <div id="collapse7" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="">HAS THE CHILDREN SUFFERES FROM ANY DISEASE? IF YES GIVE DETAILS</label>
                                                            <textarea class="form-control" rows="3" id="any_disease" name="any_disease_Edit" >{{$student->any_disease}}</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">HEARING: IS IT NORMAL? IF NO GIVE DETAILS</label>
                                                            <textarea class="form-control" rows="3" id="hearing" name="hearing_Edit" >{{$student->hearing}}</textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="">EYE SIGHT: IS IT NORMAL? IF NO GIVE DETAILS</label>
                                                            <textarea class="form-control" rows="3" id="eyesight" name="eyesight_Edit" > {{$student->eyesight}}</textarea>
                                                        </div>

                                                    </div>
                                                    <div class="col-md-6">

                                                        <div class="form-group">
                                                            <label for="" style="color:red">SIGNATURE*</label>
                                                            <input type="text" id="signature" class="form-control" name="signature_Edit" value="{{$student->signature}}">
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="" style="color:red">DATE*</label>
                                                            <input type="date" id="signaturedate" class="form-control" name="signaturedate_Edit" value="{{$student->signaturedate}}">
                                                        </div>


                                                    </div>
                                                </div>
                                            </div>
                                        </section>
                                    </div>

                                </div>
                                <button type="submit" class=" btn btn-success" style="float: right">Update</button>

                            </form>
                        </section>
                    </section>

                </section>

            </section>
        </section>



    </section>

    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->

    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    <!-- file input -->
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    <!-- combodate -->
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>
    <!-- markdown -->
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/epiceditor.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>

    <script src="appassets/js/calendar/bootstrap_calendar.js"></script>
    <script src="appassets/js/calendar/demo.js"></script>
    <script src="appassets/js/sortable/jquery.sortable.js"></script>
    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader=new FileReader();

                reader.onload=function(e){
                    $('#preview_image').attr('src',e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).on('change','input[type="file"]',function(){
            readURL(this);

        })
    </script>
</body>
</html>






