<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">

    <link href="css/select2.min.css" rel="stylesheet" />
</head>
<body>
<section class="vbox">
    @include('partials.header')
    <section class="hbox stretch">
        @include('partials.navbar')
                <!-- /.aside -->
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">


                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="{{route('term')}}"><i class="fa fa-home"></i>Fees</a></li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">Fees</h3>
                                <small>Welcome back, Albert</small>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    @include('partials.messages')
                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">New</header>
                                        <div class="panel-body">
                                            <form method="post" action="savefee">
                                                {{csrf_field()}}
                                                <div class="form-group col-md-4">
                                                    <label for="">Academic Year:</label>
                                                    <select name="academicyear_id" id="academicyear_id" class="form-control" required>
                                                        <option value="">Select</option>
                                                        @foreach(\App\Academicyear::where('status','active')->get() as $s)
                                                            <option value="{{$s->id}}">{{$s->name}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">Term Name</label>
                                                    <select name="term_id" id="term_id" class="form-control" required>
                                                        {{--<option value="">Select</option>--}}
                                                        {{--@foreach($term_active as $s)--}}
                                                            {{--<option value="{{$s->id}}">{{$s->name}}</option>--}}
                                                        {{--@endforeach--}}
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-4">
                                                    <label for="">Class:</label>
                                                    <select name="class_id[]" id="class_id" class="form-control js-example-basic-multiple"  multiple="multiple" required>

                                                        @foreach(\App\Stage::all() as $s)
                                                            <option value="{{$s->id}}">{{$s->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <input type="hidden" id="number_of_items" name="number_of_items" value="0" />
                                                <table class="table table-hover table-inverse">
                                                    <span id="attention"></span>
                                                    <thead>
                                                    <tr>
                                                        <th>Fee item</th>
                                                        <th>Amount</th>

                                                        <th> <button type="button" class="btn btn-success " id="addnew" ><i class="fa fa-plus-circle" aria-hidden="true"></i></button></th>
                                                    </tr>
                                                    </thead>
                                                    <tbody id="data"></tbody>

                                                </table>

                                                <button type="submit" class="btn btn-success" style="float:right;">Submit</button>

                                            </form>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-2"></div>
                            </div>


                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>

    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->
    <script type="text/javascript" src="{{ asset('appassets/js/fuelux/fuelux.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    <!-- file input -->
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    <!-- combodate -->
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>
    <!-- markdown -->
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/epiceditor.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
    <script src="https://datatables.yajrabox.com/js/handlebars.js"></script>
    <script src="js/select2.min.js"></script>

    <script>
        $(document).ready(function() {
            $('.js-example-basic-multiple').select2();
            $('#term_id,#academicyear_id').select2();

            $(document).on('change','#academicyear_id',function(e) {
                var academicyear_id=$(this).val();
//                   alert(academicyear_id);
                $.ajax({
                    type:"GET",
                    url:"{{url('academicyeartermselectbox')}}/"+academicyear_id,
                    success: function(data) {
                        $('#term_id').append(data);
                    }
                });
            });
        });
        $(document).ready(function() {
            var currentItem = 0;
            $('#addnew').click(function(){
                currentItem =currentItem+1;
                $('#number_of_items').val(currentItem);
                var strToAdd='<tr class="item">' +
                        '<td><div class="form-group"><input name="item[]" class="form-control" type="text"  /> </div> </td> ' +
                        '<td><div class="form-group"> <input name="amount[]" class="form-control"   type="text" /> </div> </td> ' +
                        '<td><button type="button" class="btn btn-danger remove" name="remove" ><i class="fa fa-minus-circle" aria-hidden="true"></i></button> </td>'+
                        ' </tr>';
                $('#data').append(strToAdd);
            });
            $(document).on('click','.remove',function () {
                currentItem =$('#number_of_items').val() - 1;
                $('#number_of_items').val(currentItem);
                $(this).closest('tr').remove();
            })




        })

    </script>

</body>
</html>






