<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/datepicker/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">
    <link href="https://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">

</head>
<body>
<section class="vbox">
    @include('partials.header')
    <section class="hbox stretch">
        <!-- .aside -->
        @include('partials.navbar')
        <!-- /.aside -->
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">


                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="{{route('profile')}}"><i class="fa fa-home"></i> School Profile</a></li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">School Profile</h3>
                                <small>Welcome back, {{Sentinel::getUser()->first_name ." ".Sentinel::getUser()->last_name}}</small>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">
                                    @include('partials.messages')
                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">Update Profile</header>
                                        <div class="panel-body">
                                            <form method="post" action="updateschoolsetup" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <div class="vali-form">
                                                    <input type="hidden" name="id" value="{{$details->id}}"/>
                                                    <div class="form-group col-md-4">
                                                        <section class="panel panel-primary">
                                                            <div class="panel-body" >
                                                                @if(!empty($details->logo))
                                                                    <div><img id="preview_image" src="images/{{$details->logo}}" width=200px" height="300px" class="img-responsive text-center" ></div>
                                                                @else
                                                                    <div><img style="" src="images/wo.jpg" width="270px" height="270px" class="img-responsive text-center" ></div>

                                                                @endif
                                                                <br>
                                                                <br>
                                                                {{--//<form enctype="multipart/form-data"  action=""></form>--}}
                                                                <input type="file" id="imgInp" name="logo" class="form-control">
                                                                <input type="hidden" name="_token" value="{{csrf_token()}}">
                                                            </div>
                                                        </section>
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Company Name:</label>
                                                        <input type="text" class="form-control" name="companyname" value="{{$details->companyname}}" required="">
                                                    </div>


                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Phone:</label>
                                                        <input type="text" name="phone" class="form-control" value="{{$details->phone}}" required="">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Email:</label>
                                                        <input type="text" name="email" class="form-control" value="{{$details->email}}" required="">
                                                    </div>


                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Address:</label>
                                                        <input type="text" name="address" class="form-control" value="{{$details->address}}" required="">
                                                    </div>
                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Mobile:</label>
                                                        <input type="text" name="mobile"  class="form-control"value="{{$details->mobile}}" required="">
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Website:</label>
                                                        <input type="text" name="website" class="form-control" value="{{$details->website}}" required="">
                                                    </div>

                                                    <div class="form-group col-md-4">
                                                        <label class="control-label">Fax:</label>
                                                        <input type="text" name="fax" class="form-control" value="{{$details->fax}}" required="">
                                                    </div>
                                                    <div class="col-md-4 form-group" style="float:right">
                                                        <button type="submit" class="btn btn-success">Update</button>
                                                    </div>

                                                </div>






                                            </form>

                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-2"></div>
                            </div>


                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>

    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->
    <script type="text/javascript" src="{{ asset('appassets/js/fuelux/fuelux.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    <!-- file input -->
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    <!-- combodate -->
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>
    <!-- markdown -->
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/epiceditor.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
    <script src="https://datatables.yajrabox.com/js/handlebars.js"></script>
    <script>
        $(function() {
            $('#academicyear-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('allacademicyear') !!}',
                columns: [
                    { data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    { data: 'name', name: 'name' },
                    { data: 'from', name: 'from' },
                    { data: 'to', name: 'to' },
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>

</body>
</html>






