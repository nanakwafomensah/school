<aside class="bg-dark lter aside-md hidden-print hidden-xs" id="nav">
    <section class="vbox">
        <section class="w-f scrollable">
            <div class="slim-scroll" data-height="auto" data-disable-fade-out="true" data-distance="0" data-size="10px" data-color="#5cf436">
                <!-- nav -->
                <nav class="nav-primary hidden-xs">
                    <ul class="nav">

                        <li class="active">
                            <a href="dashboard" class="active"> <i class="fa fa-dashboard icon"></i> <span>Dashboard</span> </a>
                        </li>
                        <li>
                            <a href=""> <b class="badge bg-danger pull-right">13</b><i class="fa fa-newspaper-o icon"> <b class="bg-info"></b> </i> <span>Noticeboard</span> </a>
                        </li>

                        {{-- @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='admin')--}}
                        <li>
                            <a href="#layout"> <i class="fa fa-graduation-cap icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Academics</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href="#table"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> <span>Class & Subjects</span> </a>
                                    <ul class="nav bg">
                                        <li>
                                            <a href="class"> <i class="fa fa-angle-right"></i> <span>Add Class</span> </a>
                                        </li>
                                        <li>
                                            <a href="subject"> <i class="fa fa-angle-right"></i> <span>Add Subjects</span> </a>
                                        </li>


                                    </ul>
                                </li>
                                <li>
                                    <a href="{{route('teacherclass')}}"> <i class="fa fa-angle-right"></i> <span>Assign Teacher To Class</span> </a>
                                </li>
                                {{--<li>--}}
                                    {{--<a href="#table"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> <span>Terms & Semeters</span> </a>--}}
                                    {{--<ul class="nav bg">--}}
                                        {{--<li>--}}
                                            {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Add Term</span> </a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Close Semester Course</span> </a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Close Term</span> </a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href=""> <i class="fa fa-angle-right text"></i> <i class="fa fa-angle-up text-active"></i> <span>Add Class</span> </a>--}}
                                {{--</li>--}}
                                {{-- <li>
                                    <a href="{{route('term')}}"> <i class="fa fa-angle-right text"></i> <i class="fa fa-angle-up text-active"></i> <span>Add Term</span> </a>
                                </li> --}}
                                {{--<li>--}}
                                    {{--<a href=""> <i class="fa fa-angle-right text"></i> <i class="fa fa-angle-up text-active"></i> <span>Roll Over</span> </a>--}}
                                {{--</li>--}}
                                {{--<li>--}}
                                    {{--<a href="#table"> <i class="fa fa-angle-right text"></i> <i class="fa fa-angle-up text-active"></i> <span>Timetable</span> </a>--}}
                                    {{--<ul class="nav bg">--}}
                                        {{--<li>--}}
                                            {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Create Timetable</span> </a>--}}
                                        {{--</li>--}}
                                        {{--<li>--}}
                                            {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Create Exams Timetable</span> </a>--}}
                                        {{--</li>--}}
                                    {{--</ul>--}}
                                {{--</li>--}}
                            </ul>
                        </li>
                        <li>
                            <a href="#pages"> <i class="fa fa-users icon"> <b class="bg-primary dker"></b> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Users</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href="role"> <i class="fa fa-angle-right"></i> <span>Create Role</span> </a>
                                </li>

                                <li>
                                    <a href="user"> <i class="fa fa-angle-right"></i> <span>Create User</span> </a>
                                </li>

                                {{-- <li>
                                    <a href="#"> <i class="fa fa-angle-right"></i> <span>View User</span> </a>
                                </li> --}}

                                {{--<li class="">--}}
                                    {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Student Profiles</span> </a>--}}
                                {{--</li>--}}
                                {{--<li class="">--}}
                                    {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Parent Profile</span> </a>--}}
                                {{--</li>--}}
                            </ul>
                        </li>
                        {{--@endif--}}
                        {{-- STUDENT ROUTES --}}
                        <li class="">
                            <a href="#layout"> <i class="fa fa-user-o icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Student</span> </a>
                            <ul class="nav lt">
                                <li class="">
                                    <a href="{{route('newstudent')}}"> <i class="fa fa-angle-right"></i> <span>Add Student</span> </a>
                                </li>
                                <li class="">
                                    <a href="{{route('students')}}">  <i class="fa fa-angle-right"></i> <span>All Students</span> </a>
                                </li>
                                {{--<li class="">--}}
                                    {{--<a href="">  <i class="fa fa-angle-right"></i> <span>View Attendance</span> </a>--}}
                                {{--</li>--}}
                                {{--<li class="">--}}
                                    {{--<a href=""><i class="fa fa-angle-right"></i> <span>View Timetable</span> </a>--}}
                                {{--</li>--}}
                                {{--<li class="">--}}
                                    {{--<a href=""> <i class="fa fa-angle-right"></i> <span>View Noticeboard</span> </a>--}}
                                {{--</li>--}}
                                {{--<li class="">--}}
                                    {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Student Bill</span> </a>--}}
                                {{--</li>--}}
                                {{--<li class="">--}}
                                    {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Calendar</span> </a>--}}
                                {{--</li>--}}
                            </ul>
                        </li>
                        {{-- TEACHER ROUTES --}}
                        {{--  @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='teacher') --}}
                        <li>
                            <a href="#layout"> <i class="fa fa-user-circle icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Teacher</span> </a>
                            <ul class="nav lt">
                                <li class="">
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Take Student Attendance</span> </a>
                                </li>
                                <li class="">
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>View Attendance</span> </a>
                                </li>
                                <li class="">
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Input Results</span> </a>
                                </li>
                                <li class="">
                                    <a href=""><i class="fa fa-angle-right"></i> <span>View Timetable</span> </a>
                                </li>
                                 <li class="">
                                    <a href=""><i class="fa fa-angle-right"></i> <span>View Noticeboard</span> </a>
                                </li>
                                <li class="">
                                    <a href=""><i class="fa fa-angle-right"></i> <span>Student Bill</span> </a>
                                </li>
                                <li class="">
                                    <a href="">  <i class="fa fa-angle-right"></i> <span>Send Parent Message</span> </a>
                                </li>
                            </ul>
                        </li>
                        {{--@endif--}
                        {{--ADMIN AND OTHER ROUTES --}}
                        {{-- @if(Sentinel::check() && Sentinel::getUser()->roles()->first()->slug=='admin')--}}
                        {{--<li>--}}
                            {{--<a href="#layout"> <i class="fa fa-user-circle-o icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Parent</span> </a>--}}
                            {{--<ul class="nav lt">--}}
                                {{--<li class="">--}}
                                    {{--<a href="">  <i class="fa fa-angle-right"></i> <span>View Attendance</span> </a>--}}
                                {{--</li>--}}
                                {{--<li class="">--}}
                                    {{--<a href=""> <i class="fa fa-angle-right"></i> <span>View Student Results</span> </a>--}}
                                {{--</li>--}}
                                {{--<li class="">--}}
                                    {{--<a href=""><i class="fa fa-angle-right"></i> <span>View Noticeboard</span> </a>--}}
                                {{--</li>--}}
                                {{--<li class="">--}}
                                    {{--<a href=""> <i class="fa fa-angle-right"></i> <span>View Student Bill</span> </a>--}}
                                {{--</li>--}}
                                {{--<li class="">--}}
                                    {{--<a href=""> <i class="fa fa-angle-right"></i> <span>View Timetable</span> </a>--}}
                                {{--</li>--}}
                            {{--</ul>--}}
                        {{--</li>--}}
                        <li>
                            <a href="#layout"> <i class="fa fa-bar-chart icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Attendance</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href="studentattendance"> <i class="fa fa-angle-right"></i> <span>Take Student Attendance</span> </a>
                                </li>
                                {{--<li>--}}
                                    {{--<a href=""> <i class="fa fa-angle-right"></i> <span>Take Staff Attendance</span> </a>--}}
                                {{--</li>--}}
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>View Attendance</span> </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#layout"> <i class="fa fa-book icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Library</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Books</span> </a>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Add Book</span> </a>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Book Categories</span> </a>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Borrow Book</span> </a>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Return Book</span> </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#layout"> <i class="fa fa-money icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Finance</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Student Fees</span> </a>
                                    <ul class="nav bg">
                                        <li>
                                            <a href="fee"> <i class="fa fa-angle-right"></i> <span>Create Fees</span> </a>
                                        </li>
                                        <li>
                                            <a href="allfee"> <i class="fa fa-angle-right"></i> <span>View & Manage Fees</span> </a>
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Feeding Fees</span> </a>
                                    <ul class="nav bg">
                                        <li>
                                            <a href="fee"> <i class="fa fa-angle-right"></i> <span>Pay Fees</span> </a>
                                        </li>
                                        <li>
                                            <a href="allfee"> <i class="fa fa-angle-right"></i> <span>Debtors list</span> </a>
                                        </li>
                                        <li>
                                            <a href="allfee"> <i class="fa fa-angle-right"></i> <span>Report</span> </a>
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Payments</span> </a>
                                    <ul class="nav bg">
                                        <li>
                                            <a href="fee"> <i class="fa fa-angle-right"></i> <span>Pay Fees</span> </a>
                                        </li>
                                        <li>
                                            <a href="allfee"> <i class="fa fa-angle-right"></i> <span>Debtors list</span> </a>
                                        </li>
                                        <li>
                                            <a href="allfee"> <i class="fa fa-angle-right"></i> <span>Report</span> </a>
                                        </li>

                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#layout"> <i class="fa fa-building-o icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Reports</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Attendance</span> </a>
                                    <ul class="nav bg">
                                        <li>
                                            <a href="{{route('allstudentattendanceview')}}"> <i class="fa fa-angle-right"></i> <span>Students</span> </a>
                                        </li>

                                    </ul>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Students</span> </a>
                                </li>


                            </ul>
                        </li>
                        <li>
                            <a href="#layout"> <i class="fa fa-bus icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Transportation</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Assign Student To Route</span> </a>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Add Route</span> </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#layout"> <i class="fa fa-language icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Exams</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Set Grades</span> </a>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Exam Attendance</span> </a>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Approve Result</span> </a>
                                </li>
                                <li>
                                    <a href=""> <i class="fa fa-angle-right"></i> <span>Exam Results</span> </a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#layout"> <i class="fa fa-envelope-o"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Message</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href="#table"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> <span>SMS</span> </a>
                                    <ul class="nav bg">
                                        <li>
                                            <a href=""> <i class="fa fa-angle-right"></i> <span>Inbox</span> </a>
                                        </li>
                                        <li>
                                            <a href=""> <i class="fa fa-angle-right"></i> <span>Send SMS</span> </a>
                                        </li>
                                        <li>
                                            <a href=""> <i class="fa fa-angle-right"></i> <span>SMS Notification Settings</span> </a>
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#table"> <i class="fa fa-angle-right text"></i> <i class="fa fa-angle-up text-active"></i> <span>Email</span> </a>
                                    <ul class="nav bg">
                                        <li>
                                            <a href=""> <i class="fa fa-angle-right"></i> <span>Send Email</span> </a>
                                        </li>
                                        <li>
                                            <a href=""> <i class="fa fa-angle-right"></i> <span>Email Notification Settings</span> </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>

                        <li>
                            <a href="#layout"> <i class="fa fa-graduation-cap icon"> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>School Configuration</span>  </a>
                            <ul class="nav lt">
                                <li>
                                    <a href="profile"> <i class="fa fa-angle-right text"></i> <i class="fa fa-angle-up text-active"></i> <span>School Setup</span> </a>
                                </li>
                                <li>
                                    <a href="#table"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> <span>Academic Years & Calendars</span> </a>
                                    <ul class="nav bg">
                                        <li>
                                            <a href="{{route('academicyear')}}"> <i class="fa fa-angle-right"></i> <span>Academic Years</span> </a>
                                        </li>
                                        <li>
                                            <a href="{{route('term')}}"> <i class="fa fa-angle-right"></i> <span>Term</span> </a>
                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        {{--<li>
                            <a href="{{route('school-setup')}}"> <i class="fa fa-plug icon"> <b class="bg-success"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>School Setup</span> </a>
                        </li> --}}
                        {{-- <li>
                            <a href="#pages"> <i class="fa fa-cog icon"> <b class="bg-primary dker"></b> <b class="bg-primary"></b> </i> <span class="pull-right"> <i class="fa fa-angle-down text"></i> <i class="fa fa-angle-up text-active"></i> </span> <span>Configurations</span> </a>
                            <ul class="nav lt">
                                <li>
                                    <a href="signin.html"> <i class="fa fa-angle-right"></i> <span>Signin</span> </a>
                                </li>
                                <li>
                                    <a href="signup.html"> <i class="fa fa-angle-right"></i> <span>Signup</span> </a>
                                </li>
                            </ul>
                        </li> --}}
                        {{--@endif--}}
                    </ul>
                </nav>
                <!-- / nav -->
            </div>
        </section>
        <footer class="footer lt hidden-xs b-t b-dark">
            <div id="chat" class="dropup">
                <section class="dropdown-menu on aside-md m-l-n">
                    <section class="panel bg-white">
                        <header class="panel-heading b-b b-light">Active chats</header>
                        <div class="panel-body animated fadeInRight">
                            <p class="text-sm">No active chats.</p>
                            <p><a href="#" class="btn btn-sm btn-default">Start a chat</a></p>
                        </div>
                    </section>
                </section>
            </div>
            <div id="invite" class="dropup">
                <section class="dropdown-menu on aside-md m-l-n">
                    <section class="panel bg-white">
                        <header class="panel-heading b-b b-light"> John <i class="fa fa-circle text-success"></i></header>
                    </section>
                </section>
            </div>
            <a href="#nav" data-toggle="class:nav-xs" class="pull-right btn btn-sm btn-dark btn-icon"> <i class="fa fa-angle-left text"></i> <i class="fa fa-angle-right text-active"></i> </a>
        </footer>
    </section>
</aside>