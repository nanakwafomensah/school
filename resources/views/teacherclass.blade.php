<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/datepicker/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">
    <link href="https://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">

</head>
<body>
<section class="vbox">
    @include('partials.header')
    <section class="hbox stretch">
        <!-- .aside -->
        @include('partials.navbar')
                <!-- /.aside -->
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">


                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="{{route('dashboard')}}"><i class="fa fa-home"></i> Assign Teacher To Class</a></li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">Assign Teacher To Class</h3>
                                <small>Welcome back, {{Sentinel::getUser()->first_name ." ".Sentinel::getUser()->last_name}}</small>
                            </div>

                            <div class="row">
                                @include('partials.messages')
                                <div class="col-md-4">

                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">New</header>
                                        <div class="panel-body">
                                            <form method="post" action="saveteacherclass">
                                                {{csrf_field()}}
                                                <div class="form-group">
                                                    <label for="">Teacher</label>
                                                    <select name="user_id" class="form-control" required>
                                                        <option value="">Select Option</option>
                                                        @foreach(\App\Helpers\AppHelper::teacheruser() as $s)
                                                            <option value="{{$s->id}}">{{$s->first_name}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div class="form-group">
                                                    <label for="">Class</label>
                                                    <select name="class_id" class="form-control" required>
                                                        <option value="">Select Option</option>
                                                        @foreach(\App\Stage::all() as $s)
                                                            <option value="{{$s->id}}">{{$s->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>

                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success" style="float: right;">Add</button>
                                                    {{--<input type="submit" class="form-control btn btn-success" value="Submit" >--}}
                                                </div>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-8">
                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">All</header>
                                        <div class="panel-body">
                                            <table class="table table-bordered " id="subject-table">
                                                <thead>
                                                <tr>
                                                    <th style="background-color: white;color: black">No.</th>
                                                    <th style="background-color: white;color: black">TEACHER</th>
                                                    <th style="background-color: white;color: black">CLASS</th>
                                                    <th style="background-color: white;color: black;width: 20%">ACTION</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </section>
                                </div>

                                <div class="modal fade" id="deletemodal" role="dialog">
                                    <div class="modal-dialog modal-sm">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Confirm Delete</h4>
                                            </div>
                                            <form action="deleteteacherclass" method="post">
                                                <div class="modal-body">
                                                    <div class="form-group">
                                                        {{csrf_field()}}
                                                        <input type="hidden" name="user_id_Delete" id="user_id_Delete">
                                                        <p>Do You Want To Delete <span id="name_Delete"></span>  From System?</p>

                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                                                    <button type="submit" id="" class="btn btn-success"> Ok</button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div id="editmodal" class="modal fade" role="dialog">
                                    <div class="modal-dialog">
                                        <!-- Modal content-->
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal">&times;</button>
                                                <h4 class="modal-title">Edit Subject Details</h4>
                                            </div>
                                            <form action="updateteacherclass" method="post">
                                                <div class="modal-body">
                                                    {{csrf_field()}}
                                                    <div class="form-group">
                                                        <label for="">Teacher</label>
                                                        <select name="user_id_Edit" id="user_id_Edit" class="form-control" required>
                                                            <option value="">Select Option</option>
                                                            @foreach(\App\Helpers\AppHelper::teacheruser() as $s)
                                                                <option value="{{$s->id}}">{{$s->first_name}}</option>
                                                            @endforeach
                                                        </select>

                                                    </div>
                                                    <div class="form-group">
                                                        <label for="">Class</label>
                                                        <select name="class_id_Edit" id="class_id_Edit" class="form-control" required>
                                                            <option value="">Select Option</option>
                                                            @foreach(\App\Stage::all() as $s)
                                                                <option value="{{$s->id}}">{{$s->name}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times" aria-hidden="true"></i> Close</button>
                                                        <button type="submit" id="" class="btn btn-success"><i class="fa fa-floppy-o" aria-hidden="true"></i> Save</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>


                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>

    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->
    <script type="text/javascript" src="{{ asset('appassets/js/fuelux/fuelux.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    <!-- file input -->
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    <!-- combodate -->
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>
    <!-- markdown -->
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/epiceditor.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
    {{--<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>--}}
    {{--<script src="https://datatables.yajrabox.com/js/handlebars.js"></script>--}}
    <script>
        $(function() {
            $('#subject-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('allteacherclass') !!}',
                columns: [
                    { data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    { data: 'teacher', name: 'teacher' },
                    { data: 'class', name: 'class' },

                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>
    <script>
        $(document).on('click','.editbtn',function(){

            $('#class_id_Edit').val($(this).data('class_id'));
            $('#user_id_Edit').val($(this).data('user_id'));



        });
    </script>
    <script>
        $(document).on('click','.deletebtn',function() {
            $('#user_id_Delete').val($(this).data('user_id'));
            $("#name_Delete").html($(this).data('user_id'));

        });
    </script>
</body>
</html>






