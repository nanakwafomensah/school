<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/datepicker/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">

</head>
<body>
<section class="vbox">
    @include('partials.header')
    <section class="hbox stretch">
        @include('partials.navbar')
                <!-- /.aside -->
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">

                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="student"><i class="fa fa-home"></i>Studentbill</a></li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">Student Bill</h3>
                                <small>Welcome back, Albert</small>
                            </div>

                            <div class="row">
                                <div class="col-md-2"></div>
                                <div class="col-md-8">

                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">Student Number:{{$studentnumber}}&nbsp;&nbsp;Student Name:{{$studentname}} &nbsp;&nbsp;Academic Year:{{$academicyear}} &nbsp;&nbsp;Term:{{$term}}</header>
                                        <div class="panel-body">
                                            <table class="table table-hover table-inverse">

                                                    <thead>
                                                    <tr>
                                                        <th>S/N</th>
                                                        <th>Fee item</th>
                                                        <th>Amount</th>
                                                     </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php $total=0; $count=1; ?>
                                                    @foreach($feeitem as $s)
                                                         <?php $total=$total + $s->amount ?>
                                                    <tr>
                                                        <td><b>{{$count++}}</b></td>
                                                        <td><b>{{$s->name}}</b></td>
                                                        <td>{{'GHS '.$s->amount}}</td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                <tfoot>
                                                <tr>
                                                    <td></td>
                                                    <td><b>TOTAL</b></td>
                                                    <td>{{'GHS '.$total}}</td>
                                                </tr>
                                                </tfoot>


                                                </table>


                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-2"></div>
                            </div>

                        </section>
                    </section>

                </section>

            </section>
        </section>



    </section>

    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->

    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    <!-- file input -->
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    <!-- combodate -->
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>
    <!-- markdown -->
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/epiceditor.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>

    <script src="appassets/js/calendar/bootstrap_calendar.js"></script>
    <script src="appassets/js/calendar/demo.js"></script>
    <script src="appassets/js/sortable/jquery.sortable.js"></script>
    <script>
        function readURL(input){
            if(input.files && input.files[0]){
                var reader=new FileReader();

                reader.onload=function(e){
                    $('#preview_image').attr('src',e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).on('change','input[type="file"]',function(){
            readURL(this);

        })
    </script>
</body>
</html>






