<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/datepicker/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">
    {{--<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" type="text/css">--}}
    <link href="https://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">

</head>
<body>
<section class="vbox">
    @include('partials.header')
    <section class="hbox stretch">
        <!-- .aside -->
        @include('partials.navbar')
        <!-- /.aside -->
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">

                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="student"><i class="fa fa-home"></i>All Student</a></li>
                            </ul>

                            <section class="panel panel-primary">
                                <header class="panel-heading font-bold">All </header>
                                <div class="panel-body">
                                    <table class="table table-bordered " id="student-table">
                                        <thead>
                                        <tr>
                                            <th>No.</th>
                                            <th>ADM NO.</th>
                                            <th>ADM DATE.</th>
                                            <th>CHILD SURNAME</th>
                                            <th>CLASS</th>
                                            <th>GENDER</th>
                                            <th>DATE OF BIRTH</th>
                                            <th>ACTION</th>

                                        </tr>
                                        </thead>
                                        <tfoot>
                                        <tr>
                                            <td class="non_searchable"></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td class="non_searchable"></td>
                                        </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </section>

                        </section>
                    </section>

                </section>

            </section>
        </section>



    </section>

</section>
    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->

    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    {{--<!-- file input -->--}}
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    {{--<!-- combodate -->--}}
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    {{--<!-- select2 -->--}}
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    {{--<script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>--}}
    <!-- markdown -->
    {{--<script type="text/javascript" src="{{ asset('appassets/js/markdown/epiceditor.min.js') }}"></script>--}}
    {{--<script type="text/javascript" src="{{ asset('appassets/js/markdown/demo.js') }}"></script>--}}
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>

    <script src="appassets/js/calendar/bootstrap_calendar.js"></script>
    <script src="appassets/js/calendar/demo.js"></script>
    <script src="appassets/js/sortable/jquery.sortable.js"></script>

    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
    <script src="https://datatables.yajrabox.com/js/handlebars.js"></script>
    <script id="details-template" type="text/x-handlebars-template">
        @verbatim
        <table class="table">
            <tr>
                <td>Photo:</td>
                <td><img src="images/{{photo}}" width="100px" height="100px" style=" border: 1px solid #ddd;border-radius: 4px;padding: 5px;width: 150px;" class="img-responsive text-center"></td>
            </tr>
            <tr>
                <td>Full name:</td>
                <td>{{child_firstname}}  {{child_middlename}}</td>
            </tr>
            <tr>
                <td>Admission Date: </td>
                <td>{{admission_date}}</td>
            </tr>
            <tr>
                <td>Religion: </td>
                <td>{{religion}}</td>
            </tr>
            <tr>
                <td>Residential Address: </td>
                <td>{{housenumber}}</td>
            </tr>

        </table>
        @endverbatim
    </script>
    <script>
        var template = Handlebars.compile($("#details-template").html());
        var table = $('#student-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('allstudent') !!}',
            columns: [
                {
                    "className":      'details-control',
                    "orderable":      false,
                    "searchable":     false,
                    "data":           null,
                    "defaultContent": ''
                },
                {data: 'id', name: 'id'},
                {data: 'admdate', name: 'admdate'},
                {data: 'child_surname', name: 'child_surname'},
                {data: 'class', name: 'class'},
                {data: 'gender', name: 'gender'},
                {data: 'dateofbirth', name: 'dateofbirth'},
                {data: 'action', name: 'action'},
            ],
            initComplete: function () {
                this.api().columns().every(function () {
                    var column = this;

                    //example for removing search field
                    if (column.footer().className !== 'non_searchable') {
                        var input = document.createElement("input");
                        $(input).appendTo($(column.footer()).empty())
                                .keyup(function () {
                                    column.search($(this).val(), false, false, true).draw();
                                });
                    }
                });
            },
            order: [[1, 'asc']]
        });

        // Add event listener for opening and closing details
        $('#student-table tbody').on('click', 'td.details-control', function () {
            var tr = $(this).closest('tr');
            var row = table.row( tr );

            if ( row.child.isShown() ) {
                // This row is already open - close it
                row.child.hide();
                tr.removeClass('shown');
            }
            else {
                // Open this row
                row.child( template(row.data()) ).show();
                tr.addClass('shown');
            }
        });
    </script>



</body>
</html>






