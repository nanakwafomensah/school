<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/datepicker/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">
    <link href="https://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">

</head>
<body>
<section class="vbox">
    @include('partials.header')
    <section class="hbox stretch">
        @include('partials.navbar')
        <!-- /.aside -->
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">


                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="{{route('dashboard')}}"><i class="fa fa-home"></i> Role</a></li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">Role</h3>
                                <small>Welcome back, Albert</small>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    @include('partials.messages')
                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">New</header>
                                        <div class="panel-body">
                                            <form action="newrole" method="post">
                                                {{csrf_field()}}
                                                <div class="vali-form">
                                                    <div class="form-group">
                                                        <label class="control-label">Role Name</label>
                                                        <input type="text" name="name" class="form-control" required="">
                                                    </div>

                                                    <div class="clearfix"> </div>
                                                </div>


                                                <div class="col-md-12 form-group">
                                                    <button type="submit" class="btn btn-default">Submit</button>

                                                </div>
                                                <div class="clearfix"> </div>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-8">
                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">All</header>
                                        <div class="panel-body">
                                            <table class="table table-bordered " id="role-table">
                                                <thead>
                                                <tr>
                                                    <th style="background-color: white;color: black">No.</th>
                                                    <th style="background-color: white;color: black">Name</th>

                                                    <th style="background-color: white;color: black;width: 20%">Action</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </section>
                                </div>
                            </div>


                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>

    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->
    <script type="text/javascript" src="{{ asset('appassets/js/fuelux/fuelux.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    <!-- file input -->
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    <!-- combodate -->
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>
    <!-- markdown -->
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/epiceditor.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
    {{--<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>--}}
    {{--<script src="https://datatables.yajrabox.com/js/handlebars.js"></script>--}}
    <script>
        $(function() {
            $('#role-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('allroles') !!}',
                columns: [
                    { data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    { data: 'name', name: 'name' },
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ]
            });
        });
    </script>

</body>
</html>






