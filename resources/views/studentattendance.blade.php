<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/datepicker/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">
    <link href="https://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">

</head>
<body>
<section class="vbox">
    @include('partials.header')
    <section class="hbox stretch">
        <!-- .aside -->
        @include('partials.navbar')
                <!-- /.aside -->
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">
                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="{{route('dashboard')}}"><i class="fa fa-home"></i> Student Attendance</a></li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">Student Attendance</h3>
                                <small>Welcome back, {{Sentinel::getUser()->first_name ." ".Sentinel::getUser()->last_name}}</small>
                            </div>

                            <section class="panel panel-primary">
                                <header class="panel-heading font-bold">
                                    <h5 class="pull-left">Academic Year:{{\App\Academicyear::where('status','active')->first()->name}} Term:{{\App\Term::where('status','active')->first()->name}}</h5>
                                    <center><h5><b>{{$attendance_date}}</b></h5></center>

                                </header>

                                <div class="panel-body">
                                    <form method="post" action="saveattendance">

                                        {{ csrf_field() }}
                                        <input type="hidden" name="academic_year" value="1"/>
                                        <input type="hidden" name="term" value="1"/>
                                        <table class="table table-bordered " id="studentattendance-table">
                                            <thead>
                                            <tr>
                                                <th style="background-color: white;color: black"> NO.</th>
                                                <th style="background-color: white;color: black"> Student Name</th>
                                                <th style="background-color: white;color: black"> Class</th>
                                                <th style="background-color: white;color: black"> Photo</th>
                                                <th style="background-color: white;color: black"> Attendance</th>
                                            </tr>
                                            </thead>
                                            <tfoot>
                                            <tr>
                                                <td id="non_searchable"></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td id="non_searchable"></td>
                                            </tr>
                                            </tfoot>
                                        </table>
                                        <br>
                                        <div class="form-group">
                                            <input id="submit_attendance" class="btn btn-info pull-right" type="submit" name="submit_attendance"  value="Submit"/>
                                        </div>
                                    </form>

                                </div>
                            </section>

                        </section>
                    </section>

                </section>

            </section>
        </section>
    </section>

    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->
    <script type="text/javascript" src="{{ asset('appassets/js/fuelux/fuelux.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    <!-- file input -->
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    <!-- combodate -->
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>
    <!-- markdown -->
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/epiceditor.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
    {{--<script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>--}}
    {{--<script src="https://datatables.yajrabox.com/js/handlebars.js"></script>--}}
    <script>
        $(function() {
            $(document).on('click','#attendance_status',function(){
                if(this.checked){
                  $( "#attendance_statusHidden" ).prop( "disabled", true );
               }

            });


            $('#studentattendance-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('allstudentattendance') !!}',
                columns: [
                    { data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    { data: 'photo', name: 'photo' },
                    { data: 'name', name: 'name' },
                    { data: 'class', name: 'class' },
                    { data: 'attendance_action', name: 'attendance_action' },

                ],
                initComplete: function () {
                    this.api().columns().every(function () {
                        var column = this;

                        //example for removing search field
                        if (column.footer().className !== 'non_searchable') {
                            var input = document.createElement("input");
                            $(input).appendTo($(column.footer()).empty())
                                    .keyup(function () {
                                        column.search($(this).val(), false, false, true).draw();
                                    });
                        }
                    });
                }
            });
        });


    </script>


</body>
</html>






