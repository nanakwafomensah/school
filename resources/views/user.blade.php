<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/datepicker/datepicker.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">
    <link href="https://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">

</head>
<body>
<section class="vbox">
@include('partials.header')

    <section class="hbox stretch">
        <!-- .aside -->
        @include('partials.navbar')
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">


                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="{{route('user')}}"><i class="fa fa-home"></i> User</a></li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">User</h3>
                                <small>Welcome back, Albert</small>
                            </div>

                            <div class="row">
                                @include('partials.messages')
                                <div class="col-md-4">

                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">New</header>
                                        <div class="panel-body">
                                            <form action="adduser" method="post">
                                                {{csrf_field()}}
                                                <div class="vali-form">
                                                    <div class="form-group">
                                                        <label class="control-label">First Name</label>
                                                        <input type="text" name="first_name" class="form-control"  required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Last Name</label>
                                                        <input type="text" name="last_name" class="form-control"  required="">
                                                    </div>
                                                    <div class="clearfix"> </div>
                                                </div>
                                                <div class="vali-form">
                                                    <div class="form-group">
                                                        <label class="control-label">Email</label>
                                                        <input type="text" name="email" class="form-control"  required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Phone Number</label>
                                                        <input type="text" name="phonenumber" class="form-control" required="">
                                                    </div>
                                                    <div class="clearfix"> </div>
                                                </div>

                                                <div class="vali-form">
                                                    <div class="form-group">
                                                        <label class="control-label">Username</label>
                                                        <input type="text" name="username" class="form-control" required="">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="control-label">Password</label>
                                                        <input type="text" name="password" class="form-control"  required="">
                                                    </div>
                                                    <div class="clearfix"> </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Sex</label>
                                                    <select name="sex" class="form-control">
                                                        <option value="male">Male</option>
                                                        <option value="female">Female</option>
                                                    </select>
                                                </div>
                                                <div class="form-group">
                                                    <label class="control-label">Role</label>
                                                    <select name="role" class="form-control">
                                                        <option value="">Select</option>
                                                        @foreach(\Cartalyst\Sentinel\Roles\EloquentRole::all() as $s)
                                                            <option value="{{$s->name}}">{{$s->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="clearfix"> </div>

                                                <div class="col-md-12 form-group">
                                                    <button type="submit" class="btn btn-success">Submit</button>
                                                    <button type="reset" class="btn btn-default">Reset</button>
                                                </div>
                                                <div class="clearfix"> </div>
                                            </form>
                                        </div>
                                    </section>
                                </div>
                                <div class="col-md-8">
                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">All</header>
                                        <div class="panel-body">
                                            <table class="table table-bordered " id="users-table">
                                                <thead>
                                                <tr>
                                                    <th style="background-color: white;color: black">No.</th>
                                                    <th style="background-color: white;color: black">First Name</th>
                                                    <th style="background-color: white;color: black">Last Name</th>
                                                    <th style="background-color: white;color: black;">Email</th>
                                                    <th style="background-color: white;color: black;">Phone number</th>
                                                    <th style="background-color: white;color: black;">Username</th>
                                                    <th style="background-color: white;color: black;">Sex</th>
                                                    <th style="background-color: white;color: black;">Role</th>
                                                    <th style="background-color: white;color: black;">Action</th>
                                                </tr>
                                                </thead>
                                            </table>
                                        </div>
                                    </section>
                                </div>

                                <div id="editmodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <form action="{{route('edituser')}}" method="post">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Edit User Details</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <input type="hidden" id="idEdit" name="idEdit" />
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Firstname</label>
                                                                <input type="text" class="form-control"  id="firstnameEdit" name="firstnameEdit">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Lastname</label>
                                                                <input type="text" class="form-control"  id="lastnameEdit" name="lastnameEdit">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Username</label>
                                                                <input type="text" class="form-control" id="usernameEdit"  name="usernameEdit">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Email</label>
                                                                <input type="text" class="form-control" id="emailEdit"  name="emailEdit">
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Sex</label>
                                                                <select name="sexEdit" class="form-control" id="sexEdit" >
                                                                    <option value="male">Male</option>
                                                                    <option value="female">Female</option>

                                                                </select>
                                                            </div>
                                                        </div>


                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Phone number</label>
                                                                <input type="text" class="form-control" id="phonenumberEdit"  name="phonenumberEdit">
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group2 group-mail">
                                                                <label for="field-1" class="control-label">Role</label>
                                                                <select name="roleEdit" id="roleEdit" class="form-control">
                                                                    <option value="">Select</option>
                                                                    @foreach(\Cartalyst\Sentinel\Roles\EloquentRole::all() as $s)
                                                                        <option value="{{$s->id}}">{{$s->name}}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Password</label>
                                                                <input type="password" class="form-control"  id="passwordEdit"  name="passwordEdit" required>
                                                            </div>
                                                        </div>


                                                    </div>



                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button  type="submit" class="btn  waves-effect waves-light">Save changes</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.modal -->
                                <div id="deletemodal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                                    <div class="modal-dialog">
                                        <form action="{{route('deleteuser')}}" method="post">
                                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                    <h4 class="modal-title">Delete</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="row">
                                                        <div class="col-md-12">
                                                            <div class="form-group">
                                                                <label for="field-1" class="control-label">Are you sure you want to delete <span id="name_delete"></span>?</label>
                                                                <input type="hidden" class="form-control" id="idDelete" placeholder="John" name="idDelete" >
                                                            </div>
                                                        </div>

                                                    </div>

                                                </div>

                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">Close</button>
                                                    <button  type="submit" class="btn  waves-effect waves-light">Delete</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div><!-- /.modal -->

                            </div>


                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>

    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->
    <script type="text/javascript" src="{{ asset('appassets/js/fuelux/fuelux.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    <!-- file input -->
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    <!-- combodate -->
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>
    <!-- markdown -->
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/epiceditor.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/markdown/demo.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>
    <script src="https://datatables.yajrabox.com/js/datatables.bootstrap.js"></script>
    <script src="https://datatables.yajrabox.com/js/handlebars.js"></script>
    <script>
        $(function() {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('allusers') !!}',
                columns: [
                    { data: 'rownum', name: 'rownum', orderable: false, searchable: false},
                    { data: 'first_name', name: 'first_name' },
                    { data: 'last_name', name: 'last_name' },
                    { data: 'email', name: 'email' },
                    { data: 'phonenumber', name: 'phonenumber' },
                    { data: 'username', name: 'username' },
                    { data: 'sex', name: 'sex' },
                    { data: 'priviledge', name: 'priviledge' },
                    { data: 'action', name: 'action', orderable: false, searchable: false}

                ]
            });
        });
    </script>
    <script>
        $(document).on('click','.editmodal',function() {
//        alert($(this).data('priviledge'));
            $('#idEdit').val($(this).data('id'));
            $('#firstnameEdit').val($(this).data('firstname'));
            $('#lastnameEdit').val($(this).data('lastname'));
            $('#usernameEdit').val($(this).data('username'));
            $('#sexEdit').val($(this).data('sex')).change();
            $('#phonenumberEdit').val($(this).data('phonenumber'));
            $('#emailEdit').val($(this).data('email'));
            $('#roleEdit').val($(this).data('priviledge')).change();



        });
    </script>
    <script>
        $(document).on('click','.deletemodal',function() {
//        alert("Hey");

            $('#idDelete').val($(this).data('id'));


            $("#name_delete").html($(this).data('firstname')+""+$(this).data('lastname'));

        });
    </script>
</body>
</html>






