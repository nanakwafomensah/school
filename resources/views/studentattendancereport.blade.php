<!DOCTYPE html>
<html lang="en" class="app">
<head>
    <meta charset="utf-8" />
    <title>wondabyteschool | Dashboard</title>
    <meta name="description" content="app, web app, responsive, admin dashboard, admin, flat, flat ui, ui kit, off screen nav" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/select2.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/select2/theme.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/js/fuelux/fuelux.css') }}">

    <link rel="stylesheet" href="{{ URL::asset('appassets/js/slider/slider.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/app.v1.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('appassets/css/font-awesome.min.css') }}">
    <link href="https://datatables.yajrabox.com/css/datatables.bootstrap.css" rel="stylesheet">

</head>
<body>
<section class="vbox">
    @include('partials.header')
    <section class="hbox stretch">
        @include('partials.navbar')
                <!-- /.aside -->
        <section>
            <section class="hbox stretch">
                <section id="content">
                    <section class="vbox">
                        <section class="scrollable padder">


                            <ul class="breadcrumb no-border no-radius b-b b-light pull-in">
                                <li class="active"><a href="{{route('allstudentattendance')}}"><i class="fa fa-home"></i>All Students Attendance</a></li>
                            </ul>
                            <div class="m-b-md">
                                <h3 class="m-b-none">All Student Attendance Report</h3>
                                <small>Welcome back,{{Sentinel::getUser()->first_name ." ".Sentinel::getUser()->last_name}}</small>

                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    @include('partials.messages')
                                    <section class="panel panel-primary">
                                        <header class="panel-heading font-bold">New</header>
                                        <div class="panel-body">
                                            <form id="search-form">
                                                {{csrf_field()}}
                                                <div class="form-group col-md-2">
                                                    <label for="">Academic Year:</label>
                                                    <select name="academicyear" id="academicyeardropdownvalue" class="form-control" required>
                                                        <option value="">Select</option>
                                                        @foreach(\App\Academicyear::where('status','active')->get() as $s)
                                                            <option value="{{$s->id}}">{{$s->name}}</option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="">Term Name</label>
                                                    <select name="term" id="termdropdownvalue" class="form-control" required>
                                                        <option value="">Select</option>
                                                        @foreach(\App\Term::all() as $s)
                                                            <option value="{{$s->id}}">{{$s->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="">Class:</label>
                                                    <select name="stage" id="stagedropdownvalue" class="form-control "  required>
                                                        <option value="">Select</option>
                                                        @foreach(\App\Stage::all() as $s)
                                                            <option value="{{$s->id}}">{{$s->name}}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="">From:</label>
                                                    <input name="fromdate" id="fromdate" type="date"/>
                                                </div>
                                                <div class="form-group col-md-2">
                                                    <label for="">To:</label>
                                                    <input name="todate" id="todate" type="date"/>
                                                </div>
                                                <div class="col-md-2 form-group">
                                                    <button type="submit" class="btn btn-info">Search</button>

                                                </div>

                                            </form>


                                        </div>
                                        <table class="table table-bordered " id="allstudentattendancereport-table">
                                            <thead>
                                            <tr>
                                                <th>Date</th>
                                                <th>Student Admission Number</th>
                                                <th>Name</th>
                                                <th>Status</th>
                                                <th>Status</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                        </table>

                                    </section>
                                    <div class="col-md-3 form-group" style="float:right;">
                                        <a href="" id="inventoryonhandstorepdf" class="btn btn-info">Download PDF</a>
                                        <a href="" id="inventoryonhandstoreexcel" class="btn btn-info">Download CSV</a>

                                    </div>
                                </div>

                            </div>


                        </section>
                    </section>
                    <a href="#" class="hide nav-off-screen-block" data-toggle="class:nav-off-screen, open" data-target="#nav,html"></a>
                </section>
                <aside class="bg-light lter b-l aside-md hide" id="notes">
                    <div class="wrapper">Notification</div>
                </aside>
            </section>
        </section>
    </section>

    <script type="text/javascript" src="{{ asset('appassets/js/app.v1.js') }}"></script>
    <!-- fuelux -->
    <script type="text/javascript" src="{{ asset('appassets/js/fuelux/fuelux.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdn.datatables.net/1.10.12/js/jquery.dataTables.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.12/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- datepicker -->
    <script type="text/javascript" src="{{ asset('appassets/js/datepicker/bootstrap-datepicker.js') }}"></script>
    <!-- slider -->
    <script type="text/javascript" src="{{ asset('appassets/js/slider/bootstrap-slider.js') }}"></script>
    <!-- file input -->
    <script type="text/javascript" src="{{ asset('appassets/js/file-input/bootstrap-filestyle.min.js') }}"></script>
    <!-- combodate -->
    <script type="text/javascript" src="{{ asset('appassets/js/libs/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/combodate/combodate.js') }}"></script>
    <!-- select2 -->
    <script type="text/javascript" src="{{ asset('appassets/js/select2/select2.min.js') }}"></script>
    <!-- wysiwyg -->
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/jquery.hotkeys.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/bootstrap-wysiwyg.js') }}"></script>
    <script type="text/javascript" src="{{ asset('appassets/js/wysiwyg/demo.js') }}"></script>
    <!-- markdown -->
    <script type="text/javascript" src="{{ asset('appassets/js/app.plugin.js') }}"></script>
    <script src="https://datatables.yajrabox.com/js/jquery.dataTables.min.js"></script>


    <script>
        $(document).ready(function() {


            $(document).on('change','#academicyeardropdownvalue',function(e) {
                var academicyear_id=$(this).val();

                $.ajax({
                    type:"GET",
                    url:"{{url('academicyeartermselectbox')}}/"+academicyear_id,
                    success: function(data) {
                        $('#termdropdownvalue').append(data);
                    }
                });
            });
        });


    </script>
    <script>
        var academicyear;
        var stage;
        var term;

        var table =  $('#allstudentattendancereport-table').DataTable({
            dom: "<'row'<'col-xs-12'<'col-xs-6'l><'col-xs-6'p>>r>"+
            "<'row'<'col-xs-12't>>"+
            "<'row'<'col-xs-12'<'col-xs-6'i><'col-xs-6'p>>>",
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! route('allstudentattendancedata') !!}',
                data: function (d) {

                    d.academicyear = $('#academicyeardropdownvalue').val();
                    d.stage = $('#stagedropdownvalue').val();
                    d.term = $('#termdropdownvalue').val();

                }
            },
            columns: [
                {data: 'attendancedate', name: 'attendancedate'},
                {data: 'student', name: 'student'},
                {data: 'academicyear', name: 'academicyear'},
                {data: 'term', name: 'term'},
                {data: 'stage', name: 'stage'},
                {data: 'status', name: 'status'},


            ]
        });

        $('#search-form').on('submit', function(e) {
            table.draw();
            e.preventDefault();
            academicyear = $('#academicyeardropdownvalue').val();
            stage = $('#stagedropdownvalue').val();
            term = $('#termdropdownvalue').val();
        });


    </script>
</body>
</html>






